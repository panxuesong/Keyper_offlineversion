﻿using KeyperDev.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KeyperDev
{
    public delegate void loginSuccessDelegate(user u);
    /// <summary>
    /// loginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class loginWindow : Window
    {
        public loginWindow()
        {
            InitializeComponent();
            this.Hide();
            mybt_reg.Text = "注册";
            mybt_reg.blueOrRed = false;
            mybt_login.Text = "登录";
            mybt_login.blueOrRed = true;
            mybt_reg_cancel.Text = "返回";
            mybt_reg_cancel.blueOrRed = false;
            mybt_reg_ok.Text = "确认";
            mybt_reg_ok.blueOrRed = true;
             login2regst = (Storyboard)this.Resources["login2resg"];
             regst2login = (Storyboard)this.Resources["resg2login"];
        }
        #region 属性
        public userList userList = new userList();
        public loginSuccessDelegate loginSuccess;
        public Storyboard login2regst = new Storyboard();
        public Storyboard regst2login = new Storyboard();
        #endregion
        #region 方法
        public int loadUsersFromFile()
        {
            int i = userList.loadUsersFromFile();
            if (i == 0)
            {
                return i;
            }
            else { 
                
            foreach (user u in userList.userArrayList)
            {
                log_unBox.Text = u.userName;
                break;
                //userListElt ule = new userListElt();
                //ule.u = u;
                //ule.userNameBox.Text = u.userName;
                ////ule.userImage = u.userIcon;
                //userNameComboBox.Items.Add(ule);
                //userNameComboBox.SelectedIndex = 0;
            }

            }
           
            return i;
        }
        #endregion

        private void quit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

       

        private void dragtomove(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        	if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();// 在此处添加事件处理程序实现。// 在此处添加事件处理程序实现。
        }

		private int agreeflag = 0;
        private void agreeclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        	if(agreeflag == 0){
				agreeflag =1;
                agreegou.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                agreeflag = 0;
                agreegou.Visibility = System.Windows.Visibility.Hidden;
            }
			// 在此处添加事件处理程序实现。
        }

        private void back2login(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            regst2login.Begin();
            rgs_unBox.Clear();
            rgs_pw1.Clear();
            rgs_pw2.Clear();
            agreeflag = 0;
            agreegou.Visibility = System.Windows.Visibility.Hidden;
        }
        private void back2login()
        {
            if (remflag == 0)
            {
                log_pwBox.Clear();
                log_unBox.Text = rgs_unBox.Text;
            }
            regst2login.Begin();
            rgs_unBox.Clear();
            rgs_pw1.Clear();
            rgs_pw2.Clear();
            agreeflag = 0;
            agreegou.Visibility = System.Windows.Visibility.Hidden;
        }

        private void rgsclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (agreeflag == 0)
            {
                uMessageBox.Show(this,"提示","请接受许可协议！");
            }
            else
            {
                if (checkusername(rgs_unBox.Text.ToString()) == 0)
                {
                    if (checkpassword() == 0)
                    {
                        user u = new user();
                        u.userName = rgs_unBox.Text.ToString();
                        u.passwordHash = cryptogram.mySM3(rgs_pw1.Password.ToString());
                        u.fingerprintCodeHash = "null";
                        userList.userArrayList.Add(u);
                        userList.userNum++;
                        userList.writeUsersFromFile(u);
                        uMessageBox.Show(this,"提示", "注册成功！点击返回");
                        back2login();
                    }
                }
               

            }
        }
        private int checkusername(string str)
        {
            if (str.Length == 0)
            {
                uMessageBox.Show(this,"提示", "请输入用户名！");
                return -1;
            }
            else if (str.Length > 20)
            {
                uMessageBox.Show(this,"提示", "用户名过长！");
                return -1;
            }
            else {
                return 0;
            }
        }

        private int checkpassword()
        {
            if (rgs_pw1.Password.Length == 0)
            {
                uMessageBox.Show(this,"提示","请输入密码！");
                return -1;
            }else if(rgs_pw1.Password.Length >20){
                uMessageBox.Show(this,"提示", "密码过长！");
                return -1;
            }
            else if (rgs_pw1.Password.Length < 5)
            {
                uMessageBox.Show(this,"提示", "密码过短！");
                return -1;
            }
            else if (rgs_pw2.Password.Length == 0)
            {
                uMessageBox.Show(this,"提示", "请确认密码！");
                return -1;
            }
            else if (String.Compare(rgs_pw1.Password, rgs_pw2.Password) == 0)
            {
                return 0;
            }
            uMessageBox.Show(this,"提示", "密码不一致，请重新输入！");
            return -1;
        }

        public int autoflag = 0;
        private void agree_autclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (autoflag == 0)
            {
                autoflag = 1;
                agreegou_aut.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                autoflag = 0;
                agreegou_aut.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        private int remflag = 0;
        private void agree_remclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (remflag == 0)
            {
                remflag = 1;
                agreegou_rem.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                remflag = 0;
                agreegou_rem.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void loginclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (checkusername(log_unBox.Text.ToString()) != 0)
            {
                return;
            }
            else
            {
                if (log_pwBox.Password.Length == 0)
                {
                    uMessageBox.Show(this,"提示", "请输入密码！");
                    return;
                }
                foreach (user u in userList.userArrayList)
                {
                    if (String.Compare(u.userName, log_unBox.Text.ToString()) == 0)
                    {
                        if (String.Compare(u.passwordHash, cryptogram.mySM3(log_pwBox.Password)) == 0)
                        {
                            loginSuccess(u);
                            return;
                        }
                        else
                        {
                            uMessageBox.Show(this,"提示","密码错误！");
                            return;
                        }
                    }
                }
                uMessageBox.Show(this,"提示", "用户不存在！");
                return;
            }

        }

        private void torgsclick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            
            login2regst.Begin();
            
       }

        private void quit(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }



    }
}

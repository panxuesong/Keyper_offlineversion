﻿using KeyperDev.UserControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeyperDev
{
   
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            if (staticFunctions.checkFilesSafe() == false)
            {

                MessageBox.Show("程序已经损坏！请重新安装！", "出错了QAQ", MessageBoxButton.OK, MessageBoxImage.Information);
                Application.Current.Shutdown();
            }
            InitializeComponent();
            m_SyncContext = SynchronizationContext.Current;
            this.Visibility = System.Windows.Visibility.Hidden;

            clearClipboardThread = new Thread(clearClipboard);
            clearClipboardThread.Start();
            loginWin.loginSuccess = loginSuccessCallback;
            //playpanl.Height = 526;
            playpanl.Width = 526;
            sv_play.Content = playpanl;
            //socialpanl.Height = 526;
            socialpanl.Width = 526;
            sv_social.Content = socialpanl;
            //ecomipanl.Height = 526;
            ecomipanl.Width = 526;
            sv_ecomi.Content = ecomipanl;
            //workpanl.Height = 526;
            workpanl.Width = 526;
            sv_work.Content = workpanl;
            //studypanl.Height = 526;
            studypanl.Width = 526;
            sv_study.Content = studypanl;
            //shoppanl.Height = 526;
            shoppanl.Width = 526;
            sv_shop.Content = shoppanl;

            safetixingpanl.Width = 565;
            sv_safetixing.Content = safetixingpanl;
            systongzhipanl.Width = 565;
            sv_systongzhi.Content = systongzhipanl;




            Button_shengchengmima.Text = "生成密码";
            Button_shengchengmima.blueOrRed = true;

            
            #region inistory
            cg1up = (Storyboard)this.Resources["showuppwgrid"];
            cg1down = (Storyboard)this.Resources["showdownpwgrid"];
            gonecgl = (Storyboard)this.Resources["gonepwgrid"];
            toolsup = (Storyboard)this.Resources["showuptoolsgrid"];
            toolsdown = (Storyboard)this.Resources["showdowntoolsgrid"];
            gonetools = (Storyboard)this.Resources["gonetoolsgrid"];
            settingup = (Storyboard)this.Resources["showupsettinggrid"];
            settingdown = (Storyboard)this.Resources["showdownsettinggrid"];
            gonesetting = (Storyboard)this.Resources["gonesettinggrid"];
            msgup = (Storyboard)this.Resources["showupmsggrid"];
            msgdown = (Storyboard)this.Resources["showdownmsggrid"];
            gonemsg = (Storyboard)this.Resources["gonemsggrid"];
            pwgridchange1 = (Storyboard)this.Resources["pwgridchange1"];
            pwgridchange2 = (Storyboard)this.Resources["pwgridchange2"];
            pwgridchange_1 = (Storyboard)this.Resources["pwgridchange_1"];
            pwgridchange_2 = (Storyboard)this.Resources["pwgridchange_2"];
            mima2tishi = (Storyboard)this.Resources["mima2tishi"];
            tishi2mima = (Storyboard)this.Resources["tishi2mima"];
            mima2tishi1 = (Storyboard)this.Resources["mima2tishi1"];
            tishi2mima1 = (Storyboard)this.Resources["tishi2mima1"];

            inipic();
            mainchangestate = 1;
            
            

            #endregion


            brushred = path_red.Fill;
            brushgreen = path_green.Fill;
            brushblue = path_blue.Fill;


            if (staticFunctions.checkAlreadyRun() == true)      //未实现
            {
                
                MessageBox.Show("程序已运行！", "工程模式QAQ", MessageBoxButton.OK, MessageBoxImage.Information);
                Application.Current.Shutdown();
            }
            else if (staticFunctions.getEnterProgramModel() == 1)    //未实现，读取配置文件，0表示以最小化进入程序，1表示进入登录
            {
            //进入登录界面
                int i;  
                if ((i = loginWin.loadUsersFromFile()) == 0)    //从文件中读取用户列表
                {
                    loginWin.Show();
                }
                else
                {

                    loginWin.Show();
                  //  MessageBox.Show("加载用户列表失败。\n错误代码："+i.ToString(), "出错了QAQ", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
            //最小化

            }
        }
#region  属性
        private loginWindow loginWin = new loginWindow();
        private user currentUser = new user();
        public static SynchronizationContext m_SyncContext = null;
        Brush brushred ;
        Brush brushgreen ;
        Brush brushblue;
        private userInformation userInfo = new userInformation();
        public Storyboard pwgridchange1 = new Storyboard();
        public Storyboard pwgridchange2 = new Storyboard();
        public Storyboard pwgridchange_1 = new Storyboard();
        public Storyboard pwgridchange_2 = new Storyboard();
        public Storyboard mima2tishi = new Storyboard();
        public Storyboard tishi2mima = new Storyboard();
        public Storyboard mima2tishi1 = new Storyboard();
        public Storyboard tishi2mima1 = new Storyboard();

        private MyWrapPanel socialpanl = new MyWrapPanel();
        private MyWrapPanel ecomipanl = new MyWrapPanel();
        private MyWrapPanel workpanl = new MyWrapPanel();
        private MyWrapPanel studypanl = new MyWrapPanel();
        private MyWrapPanel shoppanl = new MyWrapPanel();
        private MyWrapPanel playpanl = new MyWrapPanel();

        private MyWrapPanel safetixingpanl = new MyWrapPanel();
        private MyWrapPanel systongzhipanl = new MyWrapPanel();

        private Thread clearClipboardThread;

#endregion
        #region 指纹
        
        


        #endregion


        #region  初始化



        private void loginSuccessCallback(user u)
        {

            m_SyncContext.Post(loginSuccess,(object)u);
        }
        private void loginSuccess(object o)
        {
            currentUser = (user)o;
            userInfo.u = currentUser;
            if (Directory.Exists(userInfo.stringTObase(currentUser.userName)) == false)
                Directory.CreateDirectory(userInfo.stringTObase(currentUser.userName));

            loadMainwidowConfigFromFile();
            currentUser.fp = new Fingerprint(userInfo.stringTObase(currentUser.userName), userInfo.stringTObase("finger"));

            setMsgList();




            if (currentUser.fp.useFP == 0)
            {
                usefingerico.Visibility = System.Windows.Visibility.Hidden;
            }
            else 
            {
                usefingerico.Visibility = System.Windows.Visibility.Visible;
            }
            loadskin();
            refreshMainwidow();
           
            loginWin.Visibility = System.Windows.Visibility.Hidden;


            this.Visibility = System.Windows.Visibility.Visible;


           

        }
        private void setMsgList() 
        {
            msglistElt mle1 = new msglistElt();
            mle1.canvasevent.Visibility = System.Windows.Visibility.Visible;
            mle1.canvastixing.Visibility = System.Windows.Visibility.Hidden;
            mle1.labeltitleevent.Content = "拖库提醒";
            mle1.labellevelevent.Content = "高危";
            mle1.labeltimeevent.Content = "2017/04/12";
            mle1.tbnews.Text = "数据库泄露源：CSDN      数据泄漏量：600万用户      请及时更换密码\n      CSDN网站官方回应称，对于CSDN用户账号密码数据库被泄露一事，经过初步分析，该库系2009年CSDN作为备份所用，由于未查明原因被泄露，特向所有因此而受到影响的用户致以深深歉...";
            mle1.recbc1.Fill = brushred;
            mle1.recbc2.Fill = brushred;
            safetixingpanl.Children.Add(mle1);

            msglistElt mle2 = new msglistElt();
            mle2.canvasevent.Visibility = System.Windows.Visibility.Visible;
            mle2.canvastixing.Visibility = System.Windows.Visibility.Hidden;
            mle2.labeltitleevent.Content = "拖库提醒";
            mle2.labellevelevent.Content = "低危";
            mle2.labeltimeevent.Content = "2017/04/10";
            mle2.tbnews.Text = "数据库泄露源：EBay      数据泄漏量：1.45亿用户      \n      拍卖网站EBay表示，2月底至3月初，数据库遭到不明黑客攻击，客户的电邮地址、加密後的密码、出生日期及住址等资料被盗取，但被盗取的资料夹不含财务资料。公司促请1.45亿用户更改密码...";
            mle2.recbc1.Fill = brushgreen;
            mle2.recbc2.Fill = brushgreen;
            safetixingpanl.Children.Add(mle2);
            msglistElt mle4 = new msglistElt();
            mle4.canvasevent.Visibility = System.Windows.Visibility.Visible;
            mle4.canvastixing.Visibility = System.Windows.Visibility.Hidden;
            mle4.labeltitleevent.Content = "撞库案例";
            mle4.labellevelevent.Content = "低危";
            mle4.labeltimeevent.Content = "2017/02/21";
            mle4.tbnews.Text = "团伙高频“撞库”盗取10万余个微信号\n      据专家介绍，“撞库”利用的正是人们会使用相同的用户名和密码注册账号的习惯。黑客在互联网上搜集人们已经泄露的邮箱密码、QQ密码、各类网站账户密码等个人信息数据，建立包含用户名和密...";
            mle4.recbc1.Fill = brushgreen;
            mle4.recbc2.Fill = brushgreen;
            safetixingpanl.Children.Add(mle4);
            msglistElt mle3 = new msglistElt();
            mle3.canvasevent.Visibility = System.Windows.Visibility.Visible;
            mle3.canvastixing.Visibility = System.Windows.Visibility.Hidden;
            mle3.labeltitleevent.Content = "数据泄露";
            mle3.labellevelevent.Content = "高危";
            mle3.labeltimeevent.Content = "2017/04/10";
            mle3.tbnews.Text = "数据库泄露源：京东      数据泄漏量：千万用户      请及时更换密码\n      网传疑似京东用户数据被明码标价售卖。12月11日，京东官方确认了数据泄露的真实性并表示此次数据泄露源于2013年Struts2的安全漏洞问题，已经完成了系统修复...";
            mle3.recbc1.Fill = brushred;
            mle3.recbc2.Fill = brushred;
            safetixingpanl.Children.Add(mle3);


            msglistElt mle5 = new msglistElt();
            mle5.canvasevent.Visibility = System.Windows.Visibility.Hidden;
            mle5.canvastixing.Visibility = System.Windows.Visibility.Visible;
            mle5.labeltitletixing.Content = "密码超时";
            mle5.labelleveltixing.Content = "中危";
            mle5.labeltimetixing.Content = "2017/04/10";
            mle5.tbtixing.Text = "账号名称：QQ\n ID:5424746358\n您已46天未修改密码，请点击编辑及时修改密码！";
            mle5.recbc1.Fill = brushblue;
            mle5.recbc2.Fill = brushblue;

            systongzhipanl.Children.Add(mle5);

            msglistElt mle6= new msglistElt();
            mle6.canvasevent.Visibility = System.Windows.Visibility.Hidden;
            mle6.canvastixing.Visibility = System.Windows.Visibility.Visible;
            mle6.labeltitletixing.Content = "弱口令";
            mle6.labelleveltixing.Content = "高危";
            mle6.labeltimetixing.Content = "2017/04/10";
            mle6.tbtixing.Text = "某一账号密码强度过低，易被暴力破解。\n请点击编辑验证身份后修改密码！";
            mle6.recbc1.Fill = brushred;
            mle6.recbc2.Fill = brushred;
            systongzhipanl.Children.Add(mle6);

            
        
        }

        private int loadMainwidowConfigFromFile()
        {
            userInfo.readUserInfoFromFile(); //读取密码记录
                
                                             //读取配置文件
            return 0;
        }
        private int readTempPasswordRecords()
        {
                                            //将(程序加密的)临时密码记录添加到使用用户名加密的密码记录中
            return 0;
        }
        private int refreshMainwidow()       
        {
            usernamelabel.Text = currentUser.userName+"[已登录]";

            socialpanl.Children.Clear();
            ecomipanl.Children.Clear();
            workpanl.Children.Clear();
            studypanl.Children.Clear();
            shoppanl.Children.Clear();
            playpanl.Children.Clear();
            
            foreach (KeyValuePair<int, passwordRecord> kvp in userInfo.pwRecords.passwordDic)
            {
                        
                    pwlistElt pl = new pwlistElt();
                    pl.recordID = kvp.Key;
                    pl.accountbox.Text = "ID: " + kvp.Value.user;
                    pl.accountnamebox.Text =  kvp.Value.account + "账号";
                    pl.img.Source = pwimg5.Source;
                    pl.pwlisteltcallbackfunc += pwlisteltcallbackfunc; 
                    if (kvp.Value.passwordEncryptionLevel == 0) {
                        pl.gradebg.Fill = brushred;
                        pl.grade.Content = "A";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushred;

                    }
                    else if (kvp.Value.passwordEncryptionLevel == 1)
                    {
                        pl.gradebg.Fill = brushgreen;
                        pl.grade.Content = "B";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushgreen;
                    }
                    else if (kvp.Value.passwordEncryptionLevel == 2) 
                    {
                        pl.gradebg.Fill = brushblue;
                        pl.grade.Content = "C";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushblue;
                    }
                    MyWrapPanel wp = new MyWrapPanel();
                    if (kvp.Value.passwordType == 0)
                    {
                        socialpanl.Children.Add(pl);
                        wp = socialpanl;
                        
                    }
                    else if (kvp.Value.passwordType == 1)
                    {
                        ecomipanl.Children.Add(pl);
                        wp = ecomipanl;
                    }
                    else if (kvp.Value.passwordType == 2)
                    {
                        workpanl.Children.Add(pl);
                        wp = workpanl;
                    }
                    else if (kvp.Value.passwordType == 3)
                    {
                        studypanl.Children.Add(pl);
                        wp = studypanl;
                    }
                    else if (kvp.Value.passwordType == 4)
                    {
                        shoppanl.Children.Add(pl);
                        wp = shoppanl;
                    }
                    else if (kvp.Value.passwordType == 5)
                    {
                        playpanl.Children.Add(pl);
                        wp = playpanl;
                    }
                    if (wp.Children.Count > 3)
                        wp.Height += 130;
            }
            pwlistElt pl2 = new pwlistElt();
            pl2.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl2.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            socialpanl.Children.Add(pl2);
            pwlistElt pl7 = new pwlistElt();
            pl7.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl7.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            ecomipanl.Children.Add(pl7);
            pwlistElt pl8 = new pwlistElt();
            pl8.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl8.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            workpanl.Children.Add(pl8);
            pwlistElt pl9 = new pwlistElt();
            pl9.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl9.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            studypanl.Children.Add(pl9);
            pwlistElt pl91 = new pwlistElt();
            pl91.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl91.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            shoppanl.Children.Add(pl91);
            pwlistElt pl92 = new pwlistElt();
            pl92.pwnull.Visibility = System.Windows.Visibility.Visible;
            pl92.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
            playpanl.Children.Add(pl92);
            return 0;
        }


        private void inipic()
        {
            pimg0.Source = pwimg1.Source;
            pimg1.Source = pwimg2.Source;
            pimg2.Source = pwimg3.Source;
            pimg3.Source = pwimg4.Source;
            pimg4.Source = pwimg5.Source;
            pimg5.Source = pwimg6.Source;

        }

        #endregion

        #region mainchange







        public int mainchangestate = 0;
        public Storyboard cg1up = new Storyboard();
        public Storyboard cg1down = new Storyboard();
        public Storyboard gonecgl = new Storyboard();
        public Storyboard toolsup = new Storyboard();
        public Storyboard toolsdown = new Storyboard();
        public Storyboard gonetools = new Storyboard();
        public Storyboard settingup = new Storyboard();
        public Storyboard settingdown = new Storyboard();
        public Storyboard gonesetting = new Storyboard();
        public Storyboard msgup = new Storyboard();
        public Storyboard msgdown = new Storyboard();
        public Storyboard gonemsg = new Storyboard();

        private void change1(object sender, MouseButtonEventArgs e)
        {
            //float dpix, dpiy;
            //System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
            //dpix = graphics.DpiX;
            //dpiy = graphics.DpiY;


            //RenderTargetBitmap rtb = new RenderTargetBitmap(
            //    (int)Width, //width
            //    (int)Height, //height
            //    dpix, //dpi x
            //    dpiy, //dpi y
            //    PixelFormats.Pbgra32 // pixelformat
            //);
            //rtb.Render(this);
            //PngBitmapEncoder encode = new PngBitmapEncoder();
            //encode.Frames.Add(BitmapFrame.Create(rtb));
            //MemoryStream ms = new MemoryStream();
            //encode.Save(ms);
            //BitmapImage bim = new BitmapImage();
            //bim.BeginInit();
            //bim.StreamSource = ms;
            //bim.EndInit();
            //testimg1.Source = bim;
            //newwindow.Visibility = System.Windows.Visibility.Visible;

            change1();

        }
        public void change1()
        {

            if (mainchangestate != 1)
            {
                pwlisteli.Visibility = System.Windows.Visibility.Visible;
                if (mainchangestate > 1)
                    cg1down.Begin();
                else
                    cg1up.Begin();
                switch (mainchangestate)
                {
                    case 2:

                        toolseli.Visibility = System.Windows.Visibility.Hidden;
                        gonetools.Begin();

                        break;
                    case 3:
                        settingeli.Visibility = System.Windows.Visibility.Hidden;
                        gonesetting.Begin();
                        break;
                    case 4:
                        msgeli.Visibility = System.Windows.Visibility.Hidden;
                        gonemsg.Begin();
                        break;
                    default:
                        break;
                }

                mainchangestate = 1;
            }
        }
        private void change2(object sender, MouseButtonEventArgs e)
        {

            change2();
        }
        public void change2()
        {
            if (mainchangestate != 2)
            {
                initoolpwstrength();
                ini_tool_gpw();


                toolseli.Visibility = System.Windows.Visibility.Visible;
                if (mainchangestate > 2)
                    toolsdown.Begin();
                else
                    toolsup.Begin();
                switch (mainchangestate)
                {
                    case 1:
                        pwlisteli.Visibility = System.Windows.Visibility.Hidden;
                        gonecgl.Begin();
                        break;
                    case 3:
                        settingeli.Visibility = System.Windows.Visibility.Hidden;
                        gonesetting.Begin();
                        break;
                    case 4:
                        msgeli.Visibility = System.Windows.Visibility.Hidden;
                        gonemsg.Begin();
                        break;
                    default:
                        break;
                }

                mainchangestate = 2;

            }
        }
        private void change3(object sender, MouseButtonEventArgs e)
        {
            change3();
        }
        public void change3()
        {
            if (mainchangestate != 3)
            {
                settingeli.Visibility = System.Windows.Visibility.Visible;
                if (mainchangestate > 3)
                    settingdown.Begin();
                else
                    settingup.Begin();
                switch (mainchangestate)
                {
                    case 1:
                        pwlisteli.Visibility = System.Windows.Visibility.Hidden;
                        gonecgl.Begin();
                        break;
                    case 2:
                        toolseli.Visibility = System.Windows.Visibility.Hidden;
                        gonetools.Begin();
                        break;
                    case 4:
                        msgeli.Visibility = System.Windows.Visibility.Hidden;
                        gonemsg.Begin();
                        break;
                    default:
                        break;
                }

                mainchangestate = 3;

            }
        }
        private void change4(object sender, MouseButtonEventArgs e)
        {
            change4();
        }
        public void change4()
        {
            if (mainchangestate != 4)
            {
                msgeli.Visibility = System.Windows.Visibility.Visible;
                if (mainchangestate > 4)
                    msgdown.Begin();
                else
                    msgup.Begin();
                switch (mainchangestate)
                {
                    case 1:
                        pwlisteli.Visibility = System.Windows.Visibility.Hidden;
                        gonecgl.Begin();
                        break;
                    case 2:
                        toolseli.Visibility = System.Windows.Visibility.Hidden;
                        gonetools.Begin();
                        break;
                    case 3:
                        settingeli.Visibility = System.Windows.Visibility.Hidden;
                        gonesetting.Begin();
                        break;
                    default:
                        break;
                }

                mainchangestate = 4;

            }
        }
        #endregion

        private void SwitchStory(Canvas cs, Grid bg, double start, double end, Grid old, Grid now)
        {
            //动画定义
            Storyboard myStoryboard = new Storyboard();

            DoubleAnimationUsingKeyFrames positionDoubleAnimation = new DoubleAnimationUsingKeyFrames();

            CubicEase ce = new CubicEase();
            ce.EasingMode = EasingMode.EaseOut;

            EasingDoubleKeyFrame keyframe1 = new EasingDoubleKeyFrame();
            keyframe1.KeyTime = TimeSpan.FromSeconds(0);
            keyframe1.Value = start;

            keyframe1.EasingFunction = ce;

            EasingDoubleKeyFrame keyframe2 = new EasingDoubleKeyFrame();
            keyframe2.KeyTime = TimeSpan.FromSeconds(0.2);
            keyframe2.Value = end;
            keyframe2.EasingFunction = ce;


            positionDoubleAnimation.KeyFrames.Add(keyframe1);
            positionDoubleAnimation.KeyFrames.Add(keyframe2);


            Storyboard.SetTarget(positionDoubleAnimation, bg);  //设置动画对象

            Storyboard.SetTargetProperty(positionDoubleAnimation, new PropertyPath(Canvas.TopProperty));     //设置动画属性

            myStoryboard.Children.Add(positionDoubleAnimation);




            //开始动画
            myStoryboard.Begin(cs);
            old.Visibility = System.Windows.Visibility.Hidden;
            now.Visibility = System.Windows.Visibility.Visible;

        }

        #region pwchange
        public int endchangestate = 1;
        private int pwgridstate = 0; //0列表 1添加 2编辑 3查看 4头像添加 5头像编辑
        private bool checkpwgridstate() 
        {
            if (pwgridstate == 0) 
            {
                return true;
            }
            else if (pwgridstate == 1) 
            {
                pwbg1.Opacity = 0.01;
                pwbg2.Opacity = 0.01;
                pwbg3.Opacity = 0.01;
                pwbg4.Opacity = 0.01;
                pwbg5.Opacity = 0.01;
                pwbg6.Opacity = 0.01;
                if (uMessageBox.Show2(this,"Keyper", "是否放弃添加账号信息？选择“是”则安全退出添加。", "否", "是") == true)
                {
                    if (editflag == 1)
                        editflag = 0;
                    safecloseadd();
                    return true;
                }
                else
                    return false;
                
            }
            else if (pwgridstate == 2)
            {
                pwbg1.Opacity = 0.01;
                pwbg2.Opacity = 0.01;
                pwbg3.Opacity = 0.01;
                pwbg4.Opacity = 0.01;
                pwbg5.Opacity = 0.01;
                pwbg6.Opacity = 0.01;
                if (uMessageBox.Show2(this,"Keyper", "是否放弃编辑账号信息？选择“是”则安全退出编辑。", "否", "是") == true)
                {
                    if (editflag == 1)
                        editflag = 0;
                    safecloseadd();
                    return true;
                }
                else
                    return false;
            }
            else if (pwgridstate == 3) 
            {
                pwbg1.Opacity = 0.01;
                pwbg2.Opacity = 0.01;
                pwbg3.Opacity = 0.01;
                pwbg4.Opacity = 0.01;
                pwbg5.Opacity = 0.01;
                pwbg6.Opacity = 0.01;
                if (uMessageBox.Show2(this,"Keyper", "是否退出查看账号信息？选择“是”则安全退出查看。", "否", "是") == true)
                {
                    accountnamelabel.Content = "**********";
                    idlabel.Content = "*************";
                    classlabel.Content = "**";
                    descrptTb.Text = "***************";
                    passwordlabel.Content = "**********";
                    usedtimelabel.Content = "****";
                    edittimelabel.Content = "****************";
                    pwgridchange_1.Begin();
                    return true;
                }
                else
                    return false;
            }
            else if (pwgridstate == 4)
            {
                pwbg1.Opacity = 0.01;
                pwbg2.Opacity = 0.01;
                pwbg3.Opacity = 0.01;
                pwbg4.Opacity = 0.01;
                pwbg5.Opacity = 0.01;
                pwbg6.Opacity = 0.01;
                if (uMessageBox.Show2(this,"Keyper", "是否放弃添加账号信息？选择“是”则安全退出添加。", "否", "是") == true)
                {
                    if (editflag == 1)
                        editflag = 0;
                    pwgridchange_2.Begin();
                    safecloseadd();
                    return true;
                }
                else
                    return false;
            }
            else if (pwgridstate == 5) 
            {
                pwbg1.Opacity = 0.01;
                pwbg2.Opacity = 0.01;
                pwbg3.Opacity = 0.01;
                pwbg4.Opacity = 0.01;
                pwbg5.Opacity = 0.01;
                pwbg6.Opacity = 0.01;
                if (uMessageBox.Show2(this,"Keyper", "是否放弃编辑账号信息？选择“是”则安全退出编辑。", "否", "是") == true)
                {
                    if (editflag == 1)
                        editflag = 0;
                    pwgridchange_2.Begin();
                    safecloseadd();
                    return true;
                }
                else
                    return false;
            }

            return true;
        }
        
        private void md1(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate != 1)
            {
                if (checkpwgridstate() == false)
                    return;

                pwgridstate = 0;
                switch (endchangestate)
                {
                    
                    case 2:
                        endchangestate = 1;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg2), Canvas.GetTop(pwbg1), ecomi_grid, social_grid);
                        return;
                    case 3:
                        endchangestate = 1;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg3), Canvas.GetTop(pwbg1), work_grid, social_grid);
                        return;
                    case 4:
                        endchangestate = 1;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg4), Canvas.GetTop(pwbg1), study_grid, social_grid);
                        return;
                    case 5:
                        endchangestate = 1;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg5), Canvas.GetTop(pwbg1), shop_grid, social_grid);
                        return;
                    case 6:
                        endchangestate = 1;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg6), Canvas.GetTop(pwbg1), play_grid, social_grid);
                        return;
                }
               

            }
        }

        private void md2(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate != 2)
            {

                if (checkpwgridstate() == false)
                    return;
                pwgridstate = 0;
                switch (endchangestate)
                {
                    case 1:
                        endchangestate = 2;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg1), Canvas.GetTop(pwbg2), social_grid, ecomi_grid);
                        return;                  
                    case 3:
                        endchangestate = 2;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg3), Canvas.GetTop(pwbg2), work_grid, ecomi_grid);
                        return;
                    case 4:
                        endchangestate = 2;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg4), Canvas.GetTop(pwbg2), study_grid, ecomi_grid);
                        return;
                    case 5:
                        endchangestate = 2;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg5), Canvas.GetTop(pwbg2), shop_grid, ecomi_grid);
                        return;
                    case 6:
                        endchangestate = 2;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg6), Canvas.GetTop(pwbg2), play_grid, ecomi_grid);
                        return;
                }
                
            }
        }

        private void md3(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate != 3)
            {

                if (checkpwgridstate() == false)
                    return;
                pwgridstate = 0;
                switch (endchangestate)
                {
                    case 1:
                        endchangestate = 3;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg1), Canvas.GetTop(pwbg3), social_grid, work_grid);
                        return;
                    case 2:
                        endchangestate = 3;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg2), Canvas.GetTop(pwbg3), ecomi_grid, work_grid);
                        return;
                   
                    case 4:
                        endchangestate = 3;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg4), Canvas.GetTop(pwbg3), study_grid, work_grid);
                        return;
                    case 5:
                        endchangestate = 3;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg5), Canvas.GetTop(pwbg3), shop_grid, work_grid);
                        return;
                    case 6:
                        endchangestate = 3;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg6), Canvas.GetTop(pwbg3), play_grid, work_grid);
                        return;
                }
               
            }
        }

        private void md4(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate != 4)
            {

                if (checkpwgridstate() == false)
                    return;
                pwgridstate = 0;
                switch (endchangestate)
                {
                    case 1:
                        endchangestate = 4;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg1), Canvas.GetTop(pwbg4), social_grid, study_grid);
                        return;
                    case 2:
                        endchangestate = 4;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg2), Canvas.GetTop(pwbg4), ecomi_grid, study_grid);
                        return;
                    case 3:
                        endchangestate = 4;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg3), Canvas.GetTop(pwbg4), work_grid, study_grid);
                        return;
                  
                    case 5:
                        endchangestate = 4;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg5), Canvas.GetTop(pwbg4), shop_grid, study_grid);
                        return;
                    case 6:
                        endchangestate = 4;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg6), Canvas.GetTop(pwbg4), play_grid, study_grid);
                        return;
                }
              
            }
        }

        private void md5(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate != 5)
            {

                if (checkpwgridstate() == false)
                    return;
                pwgridstate = 0;
                switch (endchangestate)
                {
                    case 1:
                        endchangestate = 5;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg1), Canvas.GetTop(pwbg5), social_grid, shop_grid);
                        return;
                    case 2:
                        endchangestate = 5;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg2), Canvas.GetTop(pwbg5), ecomi_grid, shop_grid);
                        return;
                    case 3:
                        endchangestate = 5;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg3), Canvas.GetTop(pwbg5), work_grid, shop_grid);
                        return;
                    case 4:
                        endchangestate = 5;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg4), Canvas.GetTop(pwbg5), study_grid, shop_grid);
                        return;
                    
                    case 6:
                        endchangestate = 5;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg6), Canvas.GetTop(pwbg5), play_grid, shop_grid);
                        return;
                }
               
            }
        }

        private void md6(object sender, MouseButtonEventArgs e)
        {
            if (endchangestate !=6)
            {

                if (checkpwgridstate() == false)
                    return;
                pwgridstate = 0;
                switch (endchangestate)
                {
                    case 1:
                        endchangestate = 6;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg1), Canvas.GetTop(pwbg6), social_grid, play_grid);
                        return;
                    case 2:
                        endchangestate = 6;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg2), Canvas.GetTop(pwbg6), ecomi_grid, play_grid);
                        return;
                    case 3:
                        endchangestate = 6;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg3), Canvas.GetTop(pwbg6), work_grid, play_grid);
                        return;
                    case 4:
                        endchangestate = 6;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg4), Canvas.GetTop(pwbg6), study_grid, play_grid);
                        return;
                    case 5:
                        endchangestate = 6;
                        SwitchStory(PwbgCanvas, pwbg, Canvas.GetTop(pwbg5), Canvas.GetTop(pwbg6), shop_grid, play_grid);
                        return;
              
                }
              
            }
        }
        private void pwbg1msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.3;
            pwbg2.Opacity = 0.01;
            pwbg3.Opacity = 0.01;
            pwbg4.Opacity = 0.01;
            pwbg5.Opacity = 0.01;
            pwbg6.Opacity = 0.01;
        }
        private void pwbg2msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            pwbg2.Opacity = 0.3;
            pwbg3.Opacity = 0.01;
            pwbg4.Opacity = 0.01;
            pwbg5.Opacity = 0.01;
            pwbg6.Opacity = 0.01;
        }
        private void pwbg3msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            pwbg2.Opacity = 0.01;
            pwbg3.Opacity = 0.3;
            pwbg4.Opacity = 0.01;
            pwbg5.Opacity = 0.01;
            pwbg6.Opacity = 0.01;
        }
        private void pwbg4msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            pwbg2.Opacity = 0.01;
            pwbg3.Opacity = 0.01;
            pwbg4.Opacity = 0.3;
            pwbg5.Opacity = 0.01;
            pwbg6.Opacity = 0.01;
        }
        private void pwbg5msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            pwbg2.Opacity = 0.01;
            pwbg3.Opacity = 0.01;
            pwbg4.Opacity = 0.01;
            pwbg5.Opacity = 0.3;
            pwbg6.Opacity = 0.01;
        }
        private void pwbg6msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            pwbg2.Opacity = 0.01;
            pwbg3.Opacity = 0.01;
            pwbg4.Opacity = 0.01;
            pwbg5.Opacity = 0.01;
            pwbg6.Opacity = 0.3;
        }


        private void pwbg1mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg1.Opacity = 0.01;
            
        }
        private void pwbg2mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg2.Opacity = 0.01;
           
        }
        private void pwbg3mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg3.Opacity = 0.01;
          
        }
        private void pwbg4mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg4.Opacity = 0.01;
            
        }
        private void pwbg5mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg5.Opacity = 0.01;
           
        }
        private void pwbg6mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            pwbg6.Opacity = 0.01; 
        }
        private void pwbg1msd(object sender, MouseButtonEventArgs e)
        {
            pwbg1.Opacity = 0.5;

        }
        private void pwbg2msd(object sender, MouseButtonEventArgs e)
        {
            pwbg2.Opacity = 0.5;

        }
        private void pwbg3msd(object sender, MouseButtonEventArgs e)
        {
            pwbg3.Opacity = 0.5;

        }
        private void pwbg4msd(object sender, MouseButtonEventArgs e)
        {
            pwbg4.Opacity = 0.5;

        }
        private void pwbg5msd(object sender, MouseButtonEventArgs e)
        {
            pwbg5.Opacity = 0.5;

        }
        private void pwbg6msd(object sender, MouseButtonEventArgs e)
        {
            pwbg6.Opacity = 0.5;
        }
      

        private void selctpicback(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            pwgridchange_2.Begin();
        }

        #endregion

        #region  passwordgrid

        private void pwlisteltcallbackfunc(int addViewEditDel, int rcdid)
        {
            if (addViewEditDel == 0) //add
            {
                pwgridstate = 1;
                m_SyncContext.Post(pwlisteltcallbackfuncAdd, new object());
            }
            else if (addViewEditDel == 1) //View 
            {
                pwgridstate = 3;
                m_SyncContext.Post(pwlisteltcallbackfuncView, rcdid);
            }
            else if (addViewEditDel == 2) //edit
            {
                pwgridstate = 2;
                m_SyncContext.Post(pwlisteltcallbackfuncEdit, rcdid);
            }
            else if (addViewEditDel == 3) //del
            {
                m_SyncContext.Post(pwlisteltcallbackfuncDel, rcdid);
            }
        }
        private void pwlisteltcallbackfuncAdd(object o)
        {
            pwTypeCode = 0;
            addtitiletb.Text = "添加密码记录";
            editflag = 0;
            fg.Visibility = System.Windows.Visibility.Visible;
            fg1.Visibility = System.Windows.Visibility.Hidden;
            fg2.Visibility = System.Windows.Visibility.Hidden;
            tb_add_accountname.Clear();
            tb_add_accountID.Clear();
            tb_add_pwortishi.Clear();
            tb_add_description.Clear();
            storeTypeCode = 0;
            pwStrengthRect.Width = 0;
            pwStrengthRect.Fill = brushred;
            pwtishiLabel.Content = "";
            pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
            fg3.Visibility = System.Windows.Visibility.Visible;
            Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
            tishi2mima1.Begin();
            tb_add_pwortishi.MaxLength = 25;
            tb_add_pwortishi.MaxLines = 1;
            fg4.Visibility = System.Windows.Visibility.Hidden;
            pwStrengthRect.Visibility = System.Windows.Visibility.Visible;
          
            grid_add.Visibility = System.Windows.Visibility.Visible;
            grid_view.Visibility = System.Windows.Visibility.Hidden;

            pwgridchange1.Begin();
        }

        private void pwlisteltcallbackfuncView(object o)
        {
            int rcdid = (int)o;
            editPwID = rcdid; 
            passwordRecord pr = userInfo.pwRecords.passwordDic[rcdid];
            if (pr.passwordEncryptionLevel == 0)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将解密出A类账户密钥,是否继续?", "取消", "解密") == true)
                {
                    securitystring ss = new securitystring();
                    if (messagebox_getfingerprint.Show2(this,currentUser, pr, ss) == true)
                    {
                        if (pr.passwordEncryptionLevel == 0)
                        {
                           
                            accountnamelabel.Content = pr.account + "账号(A级加密)";
                            encryptLevelLabel.Content = "A级";
                        }
                        else if (pr.passwordEncryptionLevel == 1)
                        {
                            
                            accountnamelabel.Content = pr.account + "账号(B级加密)";
                            encryptLevelLabel.Content = "B级";
                        }
                        else if (pr.passwordEncryptionLevel == 2)
                        {
                           
                            accountnamelabel.Content = pr.account + "账号(C级加密)";
                            encryptLevelLabel.Content = "C级";
                        }
                        idlabel.Content = pr.user;
                        switch (pr.passwordType)
                        {
                            case 0:
                                classlabel.Content = "社交";
                                break;
                            case 1:
                                classlabel.Content = "金融";
                                break;
                            case 2:
                                classlabel.Content = "工作";
                                break;
                            case 3:
                                classlabel.Content = "学习";
                                break;
                            case 4:
                                classlabel.Content = "购物";
                                break;
                            case 5:
                                classlabel.Content = "娱乐";
                                break;
                            default:
                                uMessageBox.Show(this,"ERROR", "pr.passwordType = " + pr.passwordType.ToString());
                                break;
                        }
                  
                        if (pr.pwortishi == 0)
                        {
                            pwortishiLabel.Content = "密   码：";
                            #region
                            if (ss.str.Length == 0)
                            {
                                scorelabel.Content = "";

                            }
                            else 
                            {
                                int randomnessTestValue = cryptogram.randomnessTestValue(ss.str);
                                double pwStrengthRect_Width = randomnessTestValue / 100.0 * 210;
                                if (randomnessTestValue < 60)
                                {
                                    strengthrect.Fill = brushred;
                                }
                                else if (randomnessTestValue < 75)
                                {
                                    strengthrect.Fill = brushblue;
                                }
                                else if (randomnessTestValue < 100)
                                {
                                    strengthrect.Fill = brushgreen;
                                }
                                scorelabel.Content = randomnessTestValue.ToString() + "分";
                                strengthrect.Width = pwStrengthRect_Width;
                            }
                            
                            #endregion
                            pwstrengthgrid.Visibility = System.Windows.Visibility.Visible;

                        }
                        else if (pr.pwortishi == 1) 
                        {
                            pwortishiLabel.Content = "提   示：";
                            pwstrengthgrid.Visibility = System.Windows.Visibility.Hidden;
                        }
                        
                        passwordlabel.Content = ss.str;
                        descrptTb.Text = pr.description;   
                        DateTime dt = new DateTime(pr.date);
                        DateTime dtn = DateTime.Now;
                        TimeSpan ts = dtn.Subtract(dt);
                        usedtimelabel.Content = ts.Days.ToString()+"天";
                        edittimelabel.Content = dt.ToString();
                        grid_add.Visibility = System.Windows.Visibility.Hidden;
                        grid_view.Visibility = System.Windows.Visibility.Visible;

                        pwgridchange1.Begin();
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户密钥未解密！");
                        pwgridstate = 0;
                    }

                }
                else
                {
                    pwgridstate = 0;
                    return;
                }
            }
            else if (pr.passwordEncryptionLevel == 1)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将解密出B类账户密钥,是否继续?", "取消", "解密") == true)
                {
                    securitystring ss = new securitystring();
                    if (messagebox_getUserPw.Show2(this,currentUser, pr, ss) == true)
                    {
                        if (pr.passwordEncryptionLevel == 0)
                        {
                         
                            accountnamelabel.Content = pr.account + "账号(A级加密)";
                            encryptLevelLabel.Content = "A级";
                        }
                        else if (pr.passwordEncryptionLevel == 1)
                        {
                           
                            accountnamelabel.Content = pr.account + "账号(B级加密)";
                            encryptLevelLabel.Content = "B级";
                        }
                        else if (pr.passwordEncryptionLevel == 2)
                        {
                            
                            accountnamelabel.Content = pr.account + "账号(C级加密)";
                            encryptLevelLabel.Content = "C级";
                        }
                        idlabel.Content = pr.user;
                        switch (pr.passwordType)
                        {
                            case 0:
                                classlabel.Content = "社交";
                                break;
                            case 1:
                                classlabel.Content = "金融";
                                break;
                            case 2:
                                classlabel.Content = "工作";
                                break;
                            case 3:
                                classlabel.Content = "学习";
                                break;
                            case 4:
                                classlabel.Content = "购物";
                                break;
                            case 5:
                                classlabel.Content = "娱乐";
                                break;
                            default:
                                uMessageBox.Show(this,"ERROR", "pr.passwordType = " + pr.passwordType.ToString());
                                break;
                        }
                        
                        if (pr.pwortishi == 0)
                        {
                            pwortishiLabel.Content = "密   码：";
                            #region
                            if (ss.str.Length == 0)
                            {
                                scorelabel.Content = "";

                            }
                            else
                            {
                                int randomnessTestValue = cryptogram.randomnessTestValue(ss.str);
                                double pwStrengthRect_Width = randomnessTestValue / 100.0 * 210;
                                if (randomnessTestValue < 60)
                                {
                                    strengthrect.Fill = brushred;
                                }
                                else if (randomnessTestValue < 75)
                                {
                                    strengthrect.Fill = brushblue;
                                }
                                else if (randomnessTestValue < 100)
                                {
                                    strengthrect.Fill = brushgreen;
                                }
                                scorelabel.Content = randomnessTestValue.ToString() + "分";

                                strengthrect.Width = pwStrengthRect_Width;
                            }

                            #endregion
                            pwstrengthgrid.Visibility = System.Windows.Visibility.Visible;

                        }
                        else if (pr.pwortishi == 1)
                        {
                            pwortishiLabel.Content = "提   示：";
                            pwstrengthgrid.Visibility = System.Windows.Visibility.Hidden;
                        }
                        passwordlabel.Content = ss.str;
                        descrptTb.Text = pr.description;
                        DateTime dt = new DateTime(pr.date);
                        DateTime dtn = DateTime.Now;
                        TimeSpan ts = dtn.Subtract(dt);
                        usedtimelabel.Content = ts.Days.ToString() + "天";
                        edittimelabel.Content = dt.ToString();
                        grid_add.Visibility = System.Windows.Visibility.Hidden;
                        grid_view.Visibility = System.Windows.Visibility.Visible;

                        pwgridchange1.Begin();
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户密钥未解密！");
                        pwgridstate = 0;
                    }

                }
                else 
                {
                    pwgridstate = 0;
                    return;
                }
                    
            }
            else if (pr.passwordEncryptionLevel == 2)
            {


                if (pr.passwordEncryptionLevel == 0)
                {
                    
                    accountnamelabel.Content = pr.account + "账号(A级加密)";
                    encryptLevelLabel.Content = "A级";
                }
                else if (pr.passwordEncryptionLevel == 1)
                {
                    
                    accountnamelabel.Content = pr.account + "账号(B级加密)";
                    encryptLevelLabel.Content = "B级";
                }
                else if (pr.passwordEncryptionLevel == 2)
                {
                
                    accountnamelabel.Content = pr.account + "账号(C级加密)";
                    encryptLevelLabel.Content = "C级";
                }
                idlabel.Content = pr.user;
                switch (pr.passwordType)
                {
                    case 0:
                        classlabel.Content = "社交";
                        break;
                    case 1:
                        classlabel.Content = "金融";
                        break;
                    case 2:
                        classlabel.Content = "工作";
                        break;
                    case 3:
                        classlabel.Content = "学习";
                        break;
                    case 4:
                        classlabel.Content = "购物";
                        break;
                    case 5:
                        classlabel.Content = "娱乐";
                        break;
                    default:
                        uMessageBox.Show(this,"ERROR", "pr.passwordType = " + pr.passwordType.ToString());
                        break;
                }
               
                if (pr.pwortishi == 0)
                {
                    pwortishiLabel.Content = "密   码：";
                    #region
                    if (pr.encryptedPassword.Length == 0)
                    {
                        scorelabel.Content = "";

                    }
                    else
                    {
                        int randomnessTestValue = cryptogram.randomnessTestValue(pr.encryptedPassword);
                        double pwStrengthRect_Width = randomnessTestValue / 100.0 * 210;
                        if (randomnessTestValue < 60)
                        {
                            strengthrect.Fill = brushred;
                        }
                        else if (randomnessTestValue < 75)
                        {
                            strengthrect.Fill = brushblue;
                        }
                        else if (randomnessTestValue < 100)
                        {
                            strengthrect.Fill = brushgreen;
                        }
                        scorelabel.Content = randomnessTestValue.ToString() + "分";

                        strengthrect.Width = pwStrengthRect_Width;
                    }

                    #endregion
                    pwstrengthgrid.Visibility = System.Windows.Visibility.Visible;

                }
                else if (pr.pwortishi == 1)
                {
                    pwortishiLabel.Content = "提   示：";
                    pwstrengthgrid.Visibility = System.Windows.Visibility.Hidden;
                }
                passwordlabel.Content = pr.encryptedPassword;
                descrptTb.Text = pr.description;
                DateTime dt = new DateTime(pr.date);
                DateTime dtn = DateTime.Now;
                TimeSpan ts = dtn.Subtract(dt);
                usedtimelabel.Content = ts.Days.ToString() + "天";
                edittimelabel.Content = dt.ToString();
                grid_add.Visibility = System.Windows.Visibility.Hidden;
                grid_view.Visibility = System.Windows.Visibility.Visible;

                pwgridchange1.Begin();



            }

        }

        private int editflag = 0; //0表示添加模式，1表示编辑模式（未修改），2表示编辑模式（修改过）
        private void pwlisteltcallbackfuncEdit(object o)
        {
            int rcdid = (int)o;
            passwordRecord pr = userInfo.pwRecords.passwordDic[rcdid];
            editflag = 1;
            addtitiletb.Text = "编辑密码记录";
            if (pr.passwordEncryptionLevel == 0)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将解密出A类账户密钥,是否继续?", "取消", "解密") == true)
                {
                    securitystring ss = new securitystring();
                    if (messagebox_getfingerprint.Show2(this,currentUser, pr, ss) == true)
                    {
                        editflag = 1;
                        editPwID = rcdid;
                        if (pr.passwordEncryptionLevel == 0)
                        {
                            pwTypeCode = 0;
                         

                            fg.Visibility = System.Windows.Visibility.Visible;
                            fg1.Visibility = System.Windows.Visibility.Hidden;
                            fg2.Visibility = System.Windows.Visibility.Hidden;

                        }
                        else
                            uMessageBox.Show(this,"ERROR", "ERROR CODE:pr.passwordEncryptionLevel != 0 ");
                        tb_add_accountname.Text = pr.account;
                        tb_add_accountID.Text = pr.user;
                        if (String.Compare(pr.description, "无") == 0)
                            tb_add_description.Text = "";
                        else
                            tb_add_description.Text = pr.description;
                        if (pr.pwortishi == 0)
                        {
                            storeTypeCode = 0;
                            fg3.Visibility = System.Windows.Visibility.Visible;
                            Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
                            tishi2mima1.Begin();
                            tb_add_pwortishi.MaxLength = 25;
                            tb_add_pwortishi.MaxLines = 1;
                            fg4.Visibility = System.Windows.Visibility.Hidden;
                            tb_add_pwortishi.Text = ss.str;
                            #region
                            if (tb_add_pwortishi.Text.Length == 0)
                            {
                                pwtishiLabel.Content = "";
                              
                            }
                            int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                            double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                            if (randomnessTestValue < 60)
                            {
                                pwStrengthRect.Fill = brushred;
                                pwtishiLabel.Foreground = brushred;
                                pwtishiLabel.Content = "密码强度过低！";
                            }
                            else if (randomnessTestValue < 75)
                            {
                                pwStrengthRect.Fill = brushblue;
                                pwtishiLabel.Foreground = brushblue;
                                pwtishiLabel.Content = "密码强度合适！";
                            }
                            else if (randomnessTestValue < 100)
                            {
                                pwStrengthRect.Fill = brushgreen;
                                pwtishiLabel.Foreground = brushgreen;
                                pwtishiLabel.Content = "密码强度高！";
                            }
                            pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
                            pwStrengthRect.Width = pwStrengthRect_Width;
                            #endregion
                            pwStrengthRect.Visibility = System.Windows.Visibility.Visible;
                        }
                        else if (pr.pwortishi == 1)
                        {
                            storeTypeCode = 1;
                            fg3.Visibility = System.Windows.Visibility.Hidden;
                            Button_shengchengmima.Visibility = System.Windows.Visibility.Hidden;
                            mima2tishi1.Begin();
                            tb_add_pwortishi.MaxLength = 95;
                            tb_add_pwortishi.MaxLines = 3;
                            fg4.Visibility = System.Windows.Visibility.Visible;
                            tb_add_pwortishi.Text = ss.str;
                            pwtishiLabel.Content = "";
                            pwtishiLabel.Visibility = System.Windows.Visibility.Hidden;
                            pwStrengthRect.Visibility = System.Windows.Visibility.Hidden;
                        }

                        grid_add.Visibility = System.Windows.Visibility.Visible;
                        grid_view.Visibility = System.Windows.Visibility.Hidden;
                        pwgridchange1.Begin();
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户密钥未解密！");
                       
                            pwgridstate = 0;
                        
                    }
                }else
                {
                    pwgridstate = 0;
                   
                }
            }
            else if (pr.passwordEncryptionLevel == 1)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将解密出B类账户密钥,是否继续?", "取消", "解密") == true)
                {
                    securitystring ss = new securitystring();

                    if (messagebox_getUserPw.Show2(this,currentUser, pr, ss) == true)
                    {
                        editflag = 1;
                        editPwID = rcdid;
                        if (pr.passwordEncryptionLevel == 1)
                        {
                            
                            pwTypeCode = 1;
                            fg1.Visibility = System.Windows.Visibility.Visible;
                            fg.Visibility = System.Windows.Visibility.Hidden;
                            fg2.Visibility = System.Windows.Visibility.Hidden;

                        }
                        else
                            uMessageBox.Show(this,"ERROR", "ERROR CODE:pr.passwordEncryptionLevel != 1 ");
                        tb_add_accountname.Text = pr.account;
                        tb_add_accountID.Text = pr.user;
                        if (String.Compare(pr.description, "无") == 0)
                            tb_add_description.Text = "";
                        else
                            tb_add_description.Text = pr.description;
                        //descrptlabel.Content = pr.description;
                        if (pr.pwortishi == 0)
                        {
                            storeTypeCode = 0;
                            fg3.Visibility = System.Windows.Visibility.Visible;
                            Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
                  
                            tishi2mima1.Begin();
                            tb_add_pwortishi.MaxLength = 25;
                            tb_add_pwortishi.MaxLines = 1;
                            fg4.Visibility = System.Windows.Visibility.Hidden;
                            tb_add_pwortishi.Text = ss.str;
                            #region
                            if (tb_add_pwortishi.Text.Length == 0)
                            {
                                pwtishiLabel.Content = "";
                                
                            }
                            int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                            double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                            if (randomnessTestValue < 60)
                            {
                                pwStrengthRect.Fill = brushred;
                                pwtishiLabel.Foreground = brushred;
                                pwtishiLabel.Content = "密码强度过低！";
                            }
                            else if (randomnessTestValue < 75)
                            {
                                pwStrengthRect.Fill = brushblue;
                                pwtishiLabel.Foreground = brushblue;
                                pwtishiLabel.Content = "密码强度合适！";
                            }
                            else if (randomnessTestValue < 100)
                            {
                                pwStrengthRect.Fill = brushgreen;
                                pwtishiLabel.Foreground = brushgreen;
                                pwtishiLabel.Content = "密码强度高！";
                            }
                            pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
                            pwStrengthRect.Width = pwStrengthRect_Width;
                            #endregion
                            pwStrengthRect.Visibility = System.Windows.Visibility.Visible;
                        }
                        else if (pr.pwortishi == 1)
                        {
                            storeTypeCode = 1;
                            fg3.Visibility = System.Windows.Visibility.Hidden;
                            Button_shengchengmima.Visibility = System.Windows.Visibility.Hidden;
                            mima2tishi1.Begin();
         
                            tb_add_pwortishi.MaxLength = 95;
                            tb_add_pwortishi.MaxLines = 3;
                            fg4.Visibility = System.Windows.Visibility.Visible;
                            tb_add_pwortishi.Text = ss.str;
                            pwtishiLabel.Content = "";
                            pwtishiLabel.Visibility = System.Windows.Visibility.Hidden;
                            pwStrengthRect.Visibility = System.Windows.Visibility.Hidden;
                        }

                        grid_add.Visibility = System.Windows.Visibility.Visible;
                        grid_view.Visibility = System.Windows.Visibility.Hidden;
                        pwgridchange1.Begin();
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户密钥未解密！");
                   
                            pwgridstate = 0;
                    
                     
                    }
                }else
                {
                    pwgridstate = 0;
                
                }
            }
            else if (pr.passwordEncryptionLevel == 2)
            {


                editflag = 1;
                editPwID = rcdid;
                if (pr.passwordEncryptionLevel == 2)
                {
             
                    pwTypeCode = 2;
                    fg2.Visibility = System.Windows.Visibility.Visible;
                    fg1.Visibility = System.Windows.Visibility.Hidden;
                    fg.Visibility = System.Windows.Visibility.Hidden;

                }
                else
                    uMessageBox.Show(this,"ERROR", "ERROR CODE:pr.passwordEncryptionLevel != 2 ");
                tb_add_accountname.Text = pr.account;
                tb_add_accountID.Text = pr.user;
                if (String.Compare(pr.description, "无") == 0)
                    tb_add_description.Text = "";
                else
                    tb_add_description.Text = pr.description;
                //descrptlabel.Content = pr.description;
                if (pr.pwortishi == 0)
                {
                    storeTypeCode = 0;
                    fg3.Visibility = System.Windows.Visibility.Visible;
                    Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
                  
                    tishi2mima1.Begin();
                    tb_add_pwortishi.MaxLength = 25;
                    tb_add_pwortishi.MaxLines = 1;
                    fg4.Visibility = System.Windows.Visibility.Hidden;
                    tb_add_pwortishi.Text = pr.encryptedPassword;
                    #region
                    if (tb_add_pwortishi.Text.Length == 0)
                    {
                        pwtishiLabel.Content = "";
                        
                    }
                    int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                    double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                    if (randomnessTestValue < 60)
                    {
                        pwStrengthRect.Fill = brushred;
                        pwtishiLabel.Foreground = brushred;
                        pwtishiLabel.Content = "密码强度过低！";
                    }
                    else if (randomnessTestValue < 75)
                    {
                        pwStrengthRect.Fill = brushblue;
                        pwtishiLabel.Foreground = brushblue;
                        pwtishiLabel.Content = "密码强度合适！";
                    }
                    else if (randomnessTestValue < 100)
                    {
                        pwStrengthRect.Fill = brushgreen;
                        pwtishiLabel.Foreground = brushgreen;
                        pwtishiLabel.Content = "密码强度高！";
                    }

                    pwStrengthRect.Width = pwStrengthRect_Width;
                    pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
                    #endregion
                    pwStrengthRect.Visibility = System.Windows.Visibility.Visible;
                }
                else if (pr.pwortishi == 1)
                {
                    storeTypeCode = 1;
                    fg3.Visibility = System.Windows.Visibility.Hidden;
                    Button_shengchengmima.Visibility = System.Windows.Visibility.Hidden;
                    mima2tishi1.Begin();
              
                    tb_add_pwortishi.MaxLength = 95;
                    tb_add_pwortishi.MaxLines = 3;
                    fg4.Visibility = System.Windows.Visibility.Visible;
                    tb_add_pwortishi.Text = pr.encryptedPassword;
                    pwtishiLabel.Content = "";
                    pwtishiLabel.Visibility = System.Windows.Visibility.Hidden;
                    pwStrengthRect.Visibility = System.Windows.Visibility.Hidden;
                }

                grid_add.Visibility = System.Windows.Visibility.Visible;
                grid_view.Visibility = System.Windows.Visibility.Hidden;
                pwgridchange1.Begin();

            }
        }
        private void pwlisteltcallbackfuncDel(object o)
        {
            int rcdid = (int)o;
            passwordRecord pr = userInfo.pwRecords.passwordDic[rcdid];
            if (pr.passwordEncryptionLevel == 0)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将删除出A类账户,是否继续?", "取消", "删除") == true)
                {

                    if (messagebox_getfingerprint.Show3(this,currentUser) == true)
                    {
                        userInfo.delPasswordRecord(rcdid);

                        refreshpwlist(1, pr, rcdid);
                        uMessageBox.Show(this,"Keyper", "账户已删除！");
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户删除失败！");
                    }
                }
            }
            else if (pr.passwordEncryptionLevel == 1)
            {
                if (uMessageBox.Show2(this,"Keyper", "您将删除出B类账户,是否继续?", "取消", "删除") == true)
                {

                    if (messagebox_getUserPw.Show3(this,currentUser) == true)
                    {
                        userInfo.delPasswordRecord(rcdid);

                        refreshpwlist(1, pr, rcdid);
                        uMessageBox.Show(this,"Keyper", "账户已删除！");
                    }
                    else
                    {
                        uMessageBox.Show(this,"Keyper", "账户删除失败！");
                    }
                }
            }
            else if (pr.passwordEncryptionLevel == 2)
            {

                if (uMessageBox.Show2(this,"Keyper", "您将删除出C类账户，是否继续?", "取消", "删除") == true)
                {
                    userInfo.delPasswordRecord(rcdid);
                    refreshpwlist(1, pr, rcdid);
                    uMessageBox.Show(this,"Keyper", "账户已删除！");
                }

            }
        }

        private void backfromaddpw(object sender, MouseButtonEventArgs e)
        {
            if (editflag == 1)
                editflag = 0;

            safecloseadd();
         
                pwgridstate = 0;
            
        }

        private void selctpicclick(object sender, MouseButtonEventArgs e)
        {
            if(pwgridstate == 1)
            {
                pwgridstate = 4;
                
            }else if(pwgridstate == 2)
            {
                pwgridstate = 5;
             
            }
            pwgridchange2.Begin();
        }
		
		//添加条目
		private int pwTypeCode = 0;   //0表示A级，1-B，2-C

		private void clickA(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(pwTypeCode == 0)
			{
				return;
			}else
			{
                if (editflag == 1)
                    editflag = 2;
				pwTypeCode = 0;
     
				fg.Visibility = System.Windows.Visibility.Visible;
				fg1.Visibility = System.Windows.Visibility.Hidden;
				fg2.Visibility =System.Windows.Visibility.Hidden;
			}
		}

		private void clickB(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(pwTypeCode == 1)
			{
				return;
			}else
			{
                if (editflag == 1)
                    editflag = 2;
				pwTypeCode = 1;
            
				fg.Visibility = System.Windows.Visibility.Hidden;
				fg1.Visibility = System.Windows.Visibility.Visible;
				fg2.Visibility =System.Windows.Visibility.Hidden;
			}
		}

		private void clickC(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(pwTypeCode == 2)
			{
				return;
			}else
			{
                if (editflag == 1)
                    editflag = 2;
				pwTypeCode = 2;
              
				fg.Visibility = System.Windows.Visibility.Hidden;
				fg1.Visibility = System.Windows.Visibility.Hidden;
				fg2.Visibility =System.Windows.Visibility.Visible;
			}
		}
		private int storeTypeCode = 0;   //0表示存储的为密码，1表示存储的为提示
		private void clickTypePw(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(storeTypeCode == 0)
			{
				return;
			}else
			{
                if (editflag == 1)
                    editflag = 2;
                tb_add_pwortishi.Clear();
                pwStrengthRect.Width = 0;
				storeTypeCode = 0;
                pwtishiLabel.Content = "";
                pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
                tishi2mima.Begin();
                tb_add_pwortishi.MaxLength = 25;
                tb_add_pwortishi.MaxLines = 1;
				fg3.Visibility = System.Windows.Visibility.Visible;;
				fg4.Visibility = System.Windows.Visibility.Hidden;
				pwStrengthRect.Visibility = System.Windows.Visibility.Visible;   //0-282 Width
                Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
            }
			
		}

		private void clickTypeTishi(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if(storeTypeCode == 1)
			{
				return;
			}else
			{
                if (editflag == 1)
                    editflag = 2;
                tb_add_pwortishi.Clear();
				storeTypeCode = 1;
                pwtishiLabel.Visibility = System.Windows.Visibility.Hidden;
                pwtishiLabel.Content = "";
                Button_shengchengmima.Visibility = System.Windows.Visibility.Hidden;
                mima2tishi.Begin();
           
				fg3.Visibility = System.Windows.Visibility.Hidden;
               
				fg4.Visibility = System.Windows.Visibility.Visible;
				pwStrengthRect.Visibility = System.Windows.Visibility.Hidden;
			}
		}

        private void inputnow(object sender, TextChangedEventArgs e)
        {
            if (editflag == 1)
                editflag = 2;
            if (storeTypeCode == 1)
                return;
            else 
            {
                if(tb_add_pwortishi.Text.Length == 0) 
                {
                    pwtishiLabel.Content = "";
                    pwStrengthRect.Width = 0;
                    return;
                }
                if (tb_add_pwortishi.Text.Length != Encoding.Default.GetByteCount(tb_add_pwortishi.Text))
                {
                    
                   
                    uMessageBox.Show(this,"Keyper","请勿将全角字符作为密码！");
                    pwtishiLabel.Content = "";
                    pwStrengthRect.Width = 0;
                    tb_add_pwortishi.Clear();

                    return;
                }
                int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                if (randomnessTestValue < 60 ) 
                {
                    pwStrengthRect.Fill = brushred;
                    pwtishiLabel.Foreground = brushred;
                    pwtishiLabel.Content = "密码强度过低！";
                }
                else if (randomnessTestValue < 75) 
                {
                    pwStrengthRect.Fill = brushblue;
                    pwtishiLabel.Foreground = brushblue;
                    pwtishiLabel.Content = "密码强度合适！";
                }
                else if (randomnessTestValue < 100) 
                {
                    pwStrengthRect.Fill = brushgreen;
                    pwtishiLabel.Foreground = brushgreen;
                    pwtishiLabel.Content = "密码强度高！";
                }

                pwStrengthRect.Width = pwStrengthRect_Width;

                
            }
        }
        private int editPwID = -1; 
        private void bt_add_clicked(object sender, MouseButtonEventArgs e)
        {
            if (tb_add_accountname.Text.Length == 0) 
            {
                uMessageBox.Show(this,"提示","请输入账户名称，\n如“QQ”。");
                return;
            }
            else if (tb_add_accountID.Text.Length == 0) 
            {
                uMessageBox.Show(this,"提示", "请输入账户ID，\n如“123456@qq.com”。");
                return;
            }
            else if (tb_add_pwortishi.Text.Length == 0) 
            {
                if(storeTypeCode == 0)
                    uMessageBox.Show(this,"提示", "请输入密码！\n也可以选择输入密码提示。");
                else if(storeTypeCode == 1)
                    uMessageBox.Show(this,"提示", "请输入密码提示！\n");
                return;
            }

            if (editflag == 0 || editflag == 2)
            {
                #region
                if (pwTypeCode == 0)
                {
                    uMessageBox.Show(this,"验证指纹中...", "请使用KeyperU盾！");
                    passwordRecord pr = new passwordRecord();
                    pr.account = tb_add_accountname.Text;
                    pr.description = "null";
                    pr.passwordEncryptionLevel = 0;
                    pr.passwordType = endchangestate - 1;
                    pr.pwortishi = storeTypeCode;
                    pr.user = tb_add_accountID.Text;
                    if (tb_add_description.Text.Length == 0)
                    {
                        pr.description = "无";
                    }
                    else
                        pr.description = tb_add_description.Text;
                    
                    if ((messagebox_getfingerprint.Show(this,currentUser, pr, tb_add_pwortishi.Text)) == true)
                    {
                        if (editflag == 0) 
                        {
                            int i = userInfo.addPasswordRecord(pr);
                            if (i != -1)
                                refreshpwlist(0, pr, i);
                            else
                                uMessageBox.Show(this,"提示", "该条目已存在！");
                            pwgridstate = 0;
                            pwgridchange_1.Begin();
                        }
                        else if (editflag == 2) 
                        {
                            
                            userInfo.editPasswordRecord(editPwID,pr);
                            refreshpwlist(2, pr, editPwID);
                            uMessageBox.Show(this,"Keyper", "编辑成功！");
                            editflag = 0;
                            pwgridchange_1.Begin();
                           
                                pwgridstate = 0;
                             
                        }
                        
                    }
                    else
                    {
                        uMessageBox.Show(this,"提示", "账号未保存！");
                    }
                }
                else if (pwTypeCode == 1)
                {
                    uMessageBox.Show(this,"提示", "您的账号信息将保存为B级，仅受您的登录密钥保护！");
                    passwordRecord pr = new passwordRecord();
                    pr.account = tb_add_accountname.Text;
                    pr.description = "null";
                    pr.passwordEncryptionLevel = 1;
                    pr.passwordType = endchangestate - 1;
                    pr.pwortishi = storeTypeCode;
                    pr.user = tb_add_accountID.Text;
                    if (tb_add_description.Text.Length == 0)
                    {
                        pr.description = "无";
                    }
                    else
                        pr.description = tb_add_description.Text;
                   
                    if ((messagebox_getUserPw.Show(this,currentUser, pr, tb_add_pwortishi.Text)) == true)
                    {
                        if (editflag == 0)
                        {
                            int i = userInfo.addPasswordRecord(pr);
                            if (i != -1)
                                refreshpwlist(0, pr, i);
                            else
                                uMessageBox.Show(this,"提示", "该条目已存在！");
                            pwgridstate = 0;
                            pwgridchange_1.Begin();
                        }
                        else if (editflag == 2)
                        {
                            userInfo.editPasswordRecord(editPwID, pr);
                            refreshpwlist(2, pr, editPwID);
                            uMessageBox.Show(this,"Keyper", "编辑成功！");
                            editflag = 0;
                            pwgridstate = 0;
                            pwgridchange_1.Begin();
                        }
                    }
                    else
                    {
                        uMessageBox.Show(this,"提示", "账号未保存！");
                    }


                }
                else if (pwTypeCode == 2)
                {
                    if ((uMessageBox.Show2(this,"提示", "您确定将该账号保存为C级吗？\nC级保护方便查看但安全性较低！", "不保存", "保存")) == false)
                        return;
                    passwordRecord pr = new passwordRecord();
                    pr.account = tb_add_accountname.Text;
                    pr.description = "null";
                    pr.passwordEncryptionLevel = 2;
                    pr.passwordType = endchangestate - 1;
                    pr.pwortishi = storeTypeCode;
                    pr.user = tb_add_accountID.Text;
                    pr.encryptedPassword = tb_add_pwortishi.Text;
                    if (tb_add_description.Text.Length == 0)
                    {
                        pr.description = "无";
                    }
                    else
                        pr.description = tb_add_description.Text;
                    
                    if (editflag == 0)
                    {
                        int i = userInfo.addPasswordRecord(pr);
                        if (i != -1)
                            refreshpwlist(0, pr, i);
                        else
                            uMessageBox.Show(this,"提示", "该条目已存在！");
                        pwgridstate = 0;
                        pwgridchange_1.Begin();
                    }
                    else if (editflag == 2)
                    {
                        userInfo.editPasswordRecord(editPwID, pr);
                        refreshpwlist(2, pr, editPwID);
                        uMessageBox.Show(this,"Keyper", "编辑成功！");
                        editflag = 0;
                        pwgridstate = 0;
                        pwgridchange_1.Begin();
                    }

                }
                #endregion
            }
            else if (editflag == 1) 
            {
                uMessageBox.Show(this,"Keyper", "账号未修改！");
            }
          
            

        }
        private void refreshpwlist(int adddeloredit, passwordRecord pr ,int rcdid) 
        {
            
            MyWrapPanel wp = new MyWrapPanel();
            switch (pr.passwordType) 
            {
                case 0 :
                    wp = socialpanl;
                    break;
                case 1 :
                    wp = ecomipanl;
                    break;
                case 2 :
                    wp = workpanl;
                    break;
                case 3 :
                    wp = studypanl;
                    break;
                case 4 :
                    wp = shoppanl;
                    break;
                case 5 :
                    wp = playpanl;
                    break;
                default:
                    uMessageBox.Show(this,"提示", "ERROR:pr.passwordType " + pr.passwordType.ToString());
                    break;
            }
            if (adddeloredit == 0) //添加
            {
                pwlistElt pl = new pwlistElt();
                pl.accountbox.Text = "ID: " + pr.user;
                pl.accountnamebox.Text =  pr.account + "账号";
                pl.img.Source = pwimg5.Source;
                pl.recordID = rcdid;
                pl.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
                if (pr.passwordEncryptionLevel == 0)
                {
                    pl.gradebg.Fill = brushred;
                    pl.grade.Content = "A";
                    pl.pw.Visibility = System.Windows.Visibility.Visible;
                    pl.bg.Fill = brushred;

                }
                else if (pr.passwordEncryptionLevel == 1)
                {
                    pl.gradebg.Fill = brushgreen;
                    pl.grade.Content = "B";
                    pl.pw.Visibility = System.Windows.Visibility.Visible;
                    pl.bg.Fill = brushgreen;
                }
                else if (pr.passwordEncryptionLevel == 2)
                {
                    pl.gradebg.Fill = brushblue;
                    pl.grade.Content = "C";
                    pl.pw.Visibility = System.Windows.Visibility.Visible;
                    pl.bg.Fill = brushblue;
                }
                wp.Children.Insert(0,pl);
                if (wp.Children.Count > 4)
                    wp.Height += 130;
            }
            else if (adddeloredit == 1)//删除
            {
                int index = -1;
                foreach (pwlistElt p in wp.Children) 
                {
                    if (p.recordID == rcdid) 
                    {
                        index = wp.Children.IndexOf(p);
                        break;
                        
                    }
                }
                if (index > -1)
                {
                    wp.Children.RemoveAt(index);
                    if (wp.Children.Count > 3)
                        wp.Height -= 130;
                }
            }else if(adddeloredit == 2)//编辑
            {
                int index = -1;
                foreach (pwlistElt p in wp.Children) 
                {
                    if (p.recordID == rcdid) 
                    {
                        index = wp.Children.IndexOf(p);
                        break;
                        
                    }
                }
                if (index > -1)
                {
                    wp.Children.RemoveAt(index);
                    #region
                    pwlistElt pl = new pwlistElt();
                    pl.accountbox.Text = "ID: " + pr.user;
                    pl.accountnamebox.Text =  pr.account + "账号";
                    pl.img.Source = pwimg5.Source;
                    pl.recordID = rcdid;
                    pl.pwlisteltcallbackfunc += pwlisteltcallbackfunc;
                    if (pr.passwordEncryptionLevel == 0)
                    {
                        pl.gradebg.Fill = brushred;
                        pl.grade.Content = "A";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushred;

                    }
                    else if (pr.passwordEncryptionLevel == 1)
                    {
                        pl.gradebg.Fill = brushgreen;
                        pl.grade.Content = "B";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushgreen;
                    }
                    else if (pr.passwordEncryptionLevel == 2)
                    {
                        pl.gradebg.Fill = brushblue;
                        pl.grade.Content = "C";
                        pl.pw.Visibility = System.Windows.Visibility.Visible;
                        pl.bg.Fill = brushblue;
                    }
                    wp.Children.Insert(index, pl);
                    #endregion
                }
            }
        }

        private void safecloseview(object sender, MouseButtonEventArgs e)
        {
            accountnamelabel.Content = "**********";
            idlabel.Content = "*************";
            classlabel.Content = "**";
            descrptTb.Text = "***************";
            passwordlabel.Content = "**********";
            usedtimelabel.Content = "****";
            edittimelabel.Content = "****************";
            pwgridstate = 0;
            pwgridchange_1.Begin();
        }
        private void safecloseadd()
        {


            tb_add_accountname.Text = "*****";
            tb_add_accountID.Text = "**************";
            tb_add_description.Text = "***********";
            tb_add_pwortishi.Text = "******************";
            pwgridstate = 0;
            pwgridchange_1.Begin();
        }

        private void accountchanged(object sender, TextChangedEventArgs e)
        {
            if (editflag == 1)
                editflag = 2;
        }
        // private int 
        private void editfromviewclicked(object sender, MouseButtonEventArgs e)
        {
            editflag = 1;
            passwordRecord pr = userInfo.pwRecords.passwordDic[editPwID];
            addtitiletb.Text = "编辑密码记录";
            if (pr.passwordEncryptionLevel == 0)
            {
                
                pwTypeCode = 0;
                fg.Visibility = System.Windows.Visibility.Visible;
                fg1.Visibility = System.Windows.Visibility.Hidden;
                fg2.Visibility = System.Windows.Visibility.Hidden;

            }
            else if (pr.passwordEncryptionLevel == 1)
            {
                
                pwTypeCode = 1;
                fg.Visibility = System.Windows.Visibility.Hidden;
                fg1.Visibility = System.Windows.Visibility.Visible;
                fg2.Visibility = System.Windows.Visibility.Hidden;

            }
            else if (pr.passwordEncryptionLevel == 2)
            {
              
                pwTypeCode = 2;
                fg.Visibility = System.Windows.Visibility.Hidden;
                fg1.Visibility = System.Windows.Visibility.Hidden;
                fg2.Visibility = System.Windows.Visibility.Visible;

            }
            else
                uMessageBox.Show(this,"ERROR", "ERROR CODE:pr.passwordEncryptionLevel=" + pr.passwordEncryptionLevel);
            tb_add_accountname.Text = pr.account;
            tb_add_accountID.Text = pr.user;
            if (String.Compare(pr.description, "无") == 0)
                tb_add_description.Text = "";
            else
                tb_add_description.Text = pr.description;
            if (pr.pwortishi == 0)
            {
                storeTypeCode = 0;
                fg3.Visibility = System.Windows.Visibility.Visible;
                Button_shengchengmima.Visibility = System.Windows.Visibility.Visible;
                
                tishi2mima1.Begin();
                tb_add_pwortishi.MaxLength = 25;
                tb_add_pwortishi.MaxLines = 1;
                fg4.Visibility = System.Windows.Visibility.Hidden;
                tb_add_pwortishi.Text = (string)passwordlabel.Content;
                #region
                if (tb_add_pwortishi.Text.Length == 0)
                {
                    pwtishiLabel.Content = "";

                }
                int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                if (randomnessTestValue < 60)
                {
                    pwStrengthRect.Fill = brushred;
                    pwtishiLabel.Foreground = brushred;
                    pwtishiLabel.Content = "密码强度过低！";
                }
                else if (randomnessTestValue < 75)
                {
                    pwStrengthRect.Fill = brushblue;
                    pwtishiLabel.Foreground = brushblue;
                    pwtishiLabel.Content = "密码强度合适！";
                }
                else if (randomnessTestValue < 100)
                {
                    pwStrengthRect.Fill = brushgreen;
                    pwtishiLabel.Foreground = brushgreen;
                    pwtishiLabel.Content = "密码强度高！";
                }

                pwStrengthRect.Width = pwStrengthRect_Width;
                pwtishiLabel.Visibility = System.Windows.Visibility.Visible;
                #endregion
                pwStrengthRect.Visibility = System.Windows.Visibility.Visible;
            }
            else if (pr.pwortishi == 1)
            {
                storeTypeCode = 1;
                fg3.Visibility = System.Windows.Visibility.Hidden;
                Button_shengchengmima.Visibility = System.Windows.Visibility.Hidden;
                mima2tishi1.Begin();
              
                tb_add_pwortishi.MaxLength = 95;
                tb_add_pwortishi.MaxLines = 3;
                fg4.Visibility = System.Windows.Visibility.Visible;
                tb_add_pwortishi.Text = (string)passwordlabel.Content;
                pwtishiLabel.Content = "";
                pwtishiLabel.Visibility = System.Windows.Visibility.Hidden;
                pwStrengthRect.Visibility = System.Windows.Visibility.Hidden;
            }




            accountnamelabel.Content = "**********";
            idlabel.Content = "*************";
            classlabel.Content = "**";
            descrptTb.Text = "***************";
            passwordlabel.Content = "**********";
            usedtimelabel.Content = "****";
            edittimelabel.Content = "****************";
            pwgridstate = 2;
            grid_add.Visibility = System.Windows.Visibility.Visible;
            grid_view.Visibility = System.Windows.Visibility.Hidden;

        }



        #endregion

        #region 工具
        #region 工具切换

        private int toolSwitchFlag = 0;





        
        private void tool0select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (toolSwitchFlag) 
            {
                case 0:
                    return;
                case 1:
                    toolSwitchFlag = 0;
                    ini_tool_gpw();
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg1), Canvas.GetTop(toolbg0),createpw,pwstrength);
                    return;
                case 2:
                    toolSwitchFlag = 0;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg2), Canvas.GetTop(toolbg0), backup, pwstrength);
                    return;
                case 3:
                    toolSwitchFlag = 0;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg3), Canvas.GetTop(toolbg0), recovery, pwstrength);
                    return;
            }
            
        }
        private void tool1select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (toolSwitchFlag)
            {
                case 0:
                    initoolpwstrength();
                    toolSwitchFlag = 1;                   
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg0), Canvas.GetTop(toolbg1), pwstrength, createpw);
                    return;
                case 1:
                   
                    return;
                case 2:
                    toolSwitchFlag = 1;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg2), Canvas.GetTop(toolbg1), backup, createpw);
                    return;
                case 3:
                    toolSwitchFlag = 1;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg3), Canvas.GetTop(toolbg1),recovery, createpw);
                    return;
            }
        }
        private void tool2select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (toolSwitchFlag)
            {
                case 0:
                    initoolpwstrength();
                    toolSwitchFlag = 2;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg0), Canvas.GetTop(toolbg2),pwstrength, backup);
                    return;
                case 1:
                    toolSwitchFlag = 2;
                    ini_tool_gpw();
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg1), Canvas.GetTop(toolbg2),createpw, backup);
                    return;
                case 2:
                    
                    return;
                case 3:
                    toolSwitchFlag = 2;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg3), Canvas.GetTop(toolbg2), recovery, backup);
                    return;
            }
        }
        private void tool3select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (toolSwitchFlag)
            {
                case 0:
                    initoolpwstrength();
                    toolSwitchFlag = 3;
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg0), Canvas.GetTop(toolbg3), pwstrength, recovery);
                    return;
                case 1:
                    toolSwitchFlag = 3;
                    ini_tool_gpw();
                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg1), Canvas.GetTop(toolbg3),createpw, recovery);
                    return;
                case 2:
                    toolSwitchFlag = 3;

                    SwitchStory(ToolbgCanvas, toolbg, Canvas.GetTop(toolbg2), Canvas.GetTop(toolbg3),backup, recovery);
                    return;
                case 3:
                   
                    return;
            }
        }

        private void toolbg0msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg0.Opacity = 0.3;
            toolbg1.Opacity = 0.01;
            toolbg2.Opacity = 0.01;
            toolbg3.Opacity = 0.01;
        }
        private void toolbg1msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg1.Opacity = 0.3; 
            toolbg0.Opacity = 0.01;
            
            toolbg2.Opacity = 0.01;
            toolbg3.Opacity = 0.01;
        }
        private void toolbg2msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg2.Opacity = 0.3;
            toolbg0.Opacity = 0.01;
            toolbg1.Opacity = 0.01;
            
            toolbg3.Opacity = 0.01;
        }
        private void toolbg3msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg3.Opacity = 0.3;
            toolbg0.Opacity = 0.01;
            toolbg1.Opacity = 0.01;
            toolbg2.Opacity = 0.01;
        
        }
        private void toolbg0mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg0.Opacity = 0.01;
        }
        private void toolbg1mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg1.Opacity = 0.01;
        }
        private void toolbg2mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg2.Opacity = 0.01;
        }
        private void toolbg3mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            toolbg3.Opacity = 0.01;
        }
        private void tool0selectd(object sender, MouseButtonEventArgs e)
        {
            toolbg0.Opacity = 0.5;
        }
        private void tool1selectd(object sender, MouseButtonEventArgs e)
        {
            toolbg1.Opacity = 0.5;
        }
        private void tool2selectd(object sender, MouseButtonEventArgs e)
        {
            toolbg2.Opacity = 0.5;
        }
        private void tool3selectd(object sender, MouseButtonEventArgs e)
        {
            toolbg3.Opacity = 0.5;
        }
      

        #endregion

        #region pwstrength
        private void inputnow_toolpwstrength(object sender, TextChangedEventArgs e)
        {

            if (tb_tool_strengthpw.Text.Length == 0)
            {
                lable_score.Content = "";
                rec_score.Width = 0;
                tool_pwstrength_tishi.Content = "请输入待测密码！";
                return;
            }
            if (tb_tool_strengthpw.Text.Length != Encoding.Default.GetByteCount(tb_tool_strengthpw.Text))
            {


                uMessageBox.Show(this, "Keyper", "请勿将全角字符作为密码！");
                tool_pwstrength_tishi.Content = "";
                lable_score.Content = "";
                rec_score.Width = 0;
                tb_tool_strengthpw.Clear();

                return;
            }
            int randomnessTestValue = cryptogram.randomnessTestValue(tb_tool_strengthpw.Text);
            double pwStrengthRect_Width = randomnessTestValue / 100.0 * 267;
            if (randomnessTestValue < 60)
            {
                rec_score.Fill = brushred;

                tool_pwstrength_tishi.Content = "密码强度过低！";
            }
            else if (randomnessTestValue < 75)
            {
                rec_score.Fill = brushblue;

                tool_pwstrength_tishi.Content = "密码强度合适！";
            }
            else if (randomnessTestValue < 101)
            {
                rec_score.Fill = brushgreen;

                tool_pwstrength_tishi.Content = "密码强度高！";
            }
            lable_score.Content = randomnessTestValue.ToString() + "分";
            rec_score.Width = pwStrengthRect_Width;


        }
        private void initoolpwstrength() 
        {
            tb_tool_strengthpw.Clear();
            lable_score.Content = "";
            tool_pwstrength_tishi.Content = "";
            rec_score.Width = 0;
            tool_pwstrength_tishi.Content = "请输入待测密码！";
        }
        #endregion

        #region gpw
        private void me_toolgpw(object sender, MouseEventArgs e)
        {
            img_right.Opacity = 1;

        }
        private void ml_toolgpw(object sender, MouseEventArgs e)
        {
            img_right.Opacity = 0.55;
        }

        private void md_toolgpw(object sender, MouseButtonEventArgs e)
        {
            img_right.Opacity = 0.55;
        }

        private void mu_toolgpw(object sender, MouseButtonEventArgs e)
        {
            img_right.Opacity = 1;
            tb_tool_pwgenerate.Text = "";
            tool_gpw_rec_newpwStrength.Width = 0;
            if (tb_tool_Length.Text.Length == 0)
            {
                uMessageBox.Show(this, "Keyper", "请输入长度！");
               
                return;
            }
            else
            {
                try
                {
                    int i = Convert.ToInt32(tb_tool_Length.Text);
                    if (i > 5 && i < 20)
                    {
                        string pwtemp = "";
                        if (useSpecial == false)
                        {
                            pwtemp = cryptogram.getRandomPassword(i, 0);
                        }
                        else 
                        {
                            pwtemp = cryptogram.getRandomPassword(i, 1);
                        }
                        tb_tool_pwgenerate.Text = pwtemp;

                        int randomnessTestValue = cryptogram.randomnessTestValue(pwtemp);
                        double pwStrengthRect_Width = randomnessTestValue / 100.0 * 211;
                        if (randomnessTestValue < 60)
                        {
                            tool_gpw_rec_newpwStrength.Fill = brushred;
                        }
                        else if (randomnessTestValue < 75)
                        {
                            tool_gpw_rec_newpwStrength.Fill = brushblue;
                        }
                        else if (randomnessTestValue < 101)
                        {
                            tool_gpw_rec_newpwStrength.Fill = brushgreen;

                        }
                        tool_gpw_rec_newpwStrength.Width = pwStrengthRect_Width;
                        
                        return;
                    }
                    else
                    {
                        if (i < 6)
                        {
                            uMessageBox.Show(this, "Keyper", "长度不应少于6！");
                        }
                        else
                        {
                            uMessageBox.Show(this, "Keyper", "长度不应多于20！");
                        }
                        return;
                    }

                }
                catch (Exception ec)
                {
                    uMessageBox.Show(this, "Keyper", "请输入数字！");

                    tb_tool_Length.Clear();
                    return;
                }
            }


        }

        private void mu_toolgpwselectf(object sender, MouseButtonEventArgs e)
        {
            if (useSpecial == false)
            {
                useSpecial = true;
                tool_gpwselsctf.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                useSpecial = false;
                tool_gpwselsctf.Visibility = System.Windows.Visibility.Hidden;     
            }
        }

        private void mu_toolgpwselectb(object sender, MouseButtonEventArgs e)
        {
            if (useSpecial == false)
            {
                useSpecial = true;
                tool_gpwselsctf.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                useSpecial = false;
                tool_gpwselsctf.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        private bool useSpecial = false;
        private void ini_tool_gpw()
        {
            tb_tool_Length.Text = "";
            tool_gpwselsctf.Visibility = System.Windows.Visibility.Hidden;
            useSpecial = false;
            tb_tool_pwgenerate.Text = "";
            tool_gpw_rec_newpwStrength.Width = 0;
        }
        private void me_tool_gpw_copy(object sender, MouseEventArgs e)
        {
            if (tb_tool_pwgenerate.Text.Length == 0)
                return;

            tool_gpw_copy.Opacity = 1;
        }

        private void ml_tool_gpw_copy(object sender, MouseEventArgs e)
        {
            tool_gpw_copy.Opacity = 0.55;
        }

        private void md_tool_gpw_copy(object sender, MouseButtonEventArgs e)
        {
            tool_gpw_copy.Opacity = 0.55;
        }

        private void mu_tool_gpw_copy(object sender, MouseButtonEventArgs e)
        {
           
            if (tb_tool_pwgenerate.Text.Length != 0) 
            {
                tool_gpw_copy.Opacity = 1;
                tool_gpw_trycopytoclipboard();
            
            }


        }
        private void tool_gpw_trycopytoclipboard()
        {
            try
            {
                Clipboard.Clear();
                Clipboard.SetDataObject(tb_tool_pwgenerate.Text);
                //Clipboard.SetText(tb_add_pwortishi.Text);
                clearflag = 1;
                uMessageBox.Show(this, "Keyper", "密码已复制到剪切板，15s后自动销毁！可在设置中修改销毁时间。");

            }
            catch (Exception ec)
            {
                trycopytoclipboard();
            }
        }



        #endregion









        #endregion


        #region 设置
        #region 设置切换

        private int stSwitchFlag = 0;






        private void st0select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (stSwitchFlag)
            {
                case 0:
                    return;
                case 1:
                    stSwitchFlag = 0;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg1), Canvas.GetTop(stbg0), secset, comset);
                    return;
                case 2:
                    stSwitchFlag = 0;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg2), Canvas.GetTop(stbg0), charaset, comset);
                    return;
               
            }

        }
        private void st1select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (stSwitchFlag)
            {
                case 0:
                    stSwitchFlag = 1;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg0), Canvas.GetTop(stbg1), comset, secset);
                    return;
                case 1:

                    return;
                case 2:
                    stSwitchFlag = 1;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg2), Canvas.GetTop(stbg1), charaset, secset);
                    return;
             
            }
        }
        private void st2select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (stSwitchFlag)
            {
                case 0:
                    stSwitchFlag = 2;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg0), Canvas.GetTop(stbg2), comset, charaset);
                    return;
                case 1:
                    stSwitchFlag = 2;
                    SwitchStory(stbgCanvas, stbg, Canvas.GetTop(stbg1), Canvas.GetTop(stbg2), secset, charaset);
                    return;
                case 2:

                    return;
                
            }
        }
     

        private void stbg0msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg0.Opacity = 0.3;
            stbg1.Opacity = 0.01;
            stbg2.Opacity = 0.01;
          
        }
        private void stbg1msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg1.Opacity = 0.3;
            stbg0.Opacity = 0.01;
            stbg2.Opacity = 0.01;
          
        }
        private void stbg2msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg2.Opacity = 0.3;
            stbg0.Opacity = 0.01;
            stbg1.Opacity = 0.01;

           
        }
    
        private void stbg0mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg0.Opacity = 0.01;
        }
        private void stbg1mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg1.Opacity = 0.01;
        }
        private void stbg2mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stbg2.Opacity = 0.01;
        }

        private void st0selectd(object sender, MouseButtonEventArgs e)
        {
            stbg0.Opacity = 0.5;
        }
        private void st1selectd(object sender, MouseButtonEventArgs e)
        {
            stbg1.Opacity = 0.5;
        }
        private void st2selectd(object sender, MouseButtonEventArgs e)
        {
            stbg2.Opacity = 0.5;
        }


        #endregion












        #endregion



        #region 消息
        #region 消息切换

        private int msSwitchFlag = 0;






        private void ms0select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (msSwitchFlag)
            {
                case 0:
                    return;
                case 1:
                    msSwitchFlag = 0;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg1), Canvas.GetTop(msbg0), sysms, safems);
                    return;
                case 2:
                    msSwitchFlag = 0;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg2), Canvas.GetTop(msbg0), userms, safems);
                    return;

            }

        }
        private void ms1select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (msSwitchFlag)
            {
                case 0:
                    msSwitchFlag = 1;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg0), Canvas.GetTop(msbg1), safems, sysms);
                    return;
                case 1:

                    return;
                case 2:
                    msSwitchFlag = 1;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg2), Canvas.GetTop(msbg1), userms, sysms);
                    return;

            }
        }
        private void ms2select(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (msSwitchFlag)
            {
                case 0:
                    msSwitchFlag = 2;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg0), Canvas.GetTop(msbg2), safems, userms);
                    return;
                case 1:
                    msSwitchFlag = 2;
                    SwitchStory(msbgCanvas, msbg, Canvas.GetTop(msbg1), Canvas.GetTop(msbg2), sysms, userms);
                    return;
                case 2:

                    return;

            }
        }


        private void msbg0msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg0.Opacity = 0.3;
            msbg1.Opacity = 0.01;
            msbg2.Opacity = 0.01;

        }
        private void msbg1msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg1.Opacity = 0.3;
            msbg0.Opacity = 0.01;
            msbg2.Opacity = 0.01;

        }
        private void msbg2msin(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg2.Opacity = 0.3;
            msbg0.Opacity = 0.01;
            msbg1.Opacity = 0.01;


        }

        private void msbg0mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg0.Opacity = 0.01;
        }
        private void msbg1mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg1.Opacity = 0.01;
        }
        private void msbg2mslv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            msbg2.Opacity = 0.01;
        }

        private void ms0selectd(object sender, MouseButtonEventArgs e)
        {
            msbg0.Opacity = 0.5;
        }
        private void ms1selectd(object sender, MouseButtonEventArgs e)
        {
            msbg1.Opacity = 0.5;
        }
        private void ms2selectd(object sender, MouseButtonEventArgs e)
        {
            msbg2.Opacity = 0.5;
        }

       

        #endregion












        #endregion


        #region skin
        #region skinswitch
        private int skinflag = 1;

        private void selectskin1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            solveselectskin(1);
        }
        private void selectskin2(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            solveselectskin(2);
        }
        private void selectskin3(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            solveselectskin(3);
        }
        private void selectskin4(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            solveselectskin(4);
        }
        private void selectskin5(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            solveselectskin(5);
        }

        private void solveselectskin(int i) 
        {
            if (skinflag != i)
            {
                if (i != 5)
                {
                    Image img = new Image();
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();//初始化
                    using1.Visibility = System.Windows.Visibility.Hidden;
                    using2.Visibility = System.Windows.Visibility.Hidden;
                    using3.Visibility = System.Windows.Visibility.Hidden;
                    using4.Visibility = System.Windows.Visibility.Hidden;
                    using5.Visibility = System.Windows.Visibility.Hidden;
                    switch (i)
                    {
                        case 1:
                            skinflag = 1;
                            mainbg.Source = skinimg01.Source;
                            using1.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 2:
                            skinflag = 2;
                            mainbg.Source = skinimg02.Source;
                            using2.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 3:
                            skinflag = 3;
                            mainbg.Source = skinimg03.Source;
                            using3.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 4:
                            skinflag = 4;
                            mainbg.Source = skinimg04.Source;
                            using4.Visibility = System.Windows.Visibility.Visible;
                            break;

                    }


                    saveskin(i, "");
                }
                else 
                {
                    Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
                    ofd.Title = "请选择皮肤图片";
                    ofd.DefaultExt = "jpg";
                    ofd.ValidateNames = true;
                    ofd.CheckPathExists = true;
                    ofd.CheckFileExists = true;

                    if (ofd.ShowDialog() != null)
                    {
                        if (ofd.FileName != "")
                        {
                            Image img = new Image();
                            BitmapImage bmp = new BitmapImage();
                            bmp.BeginInit();//初始化


                            try
                            {
                                bmp.UriSource = new Uri(ofd.FileName);
                                bmp.EndInit();
                            }
                            catch (Exception)
                            {
                                uMessageBox.Show(this,"Keyper", "文件无法作为皮肤图片！");
                                return;
                            }
                            using1.Visibility = System.Windows.Visibility.Hidden;
                            using2.Visibility = System.Windows.Visibility.Hidden;
                            using3.Visibility = System.Windows.Visibility.Hidden;
                            using4.Visibility = System.Windows.Visibility.Hidden;
                            using5.Visibility = System.Windows.Visibility.Visible;
                            

                            mainbg.Source = bmp;
                            skinflag = 5;
                            saveskin(5, ofd.FileName);
                        }
                    }
                }
               

            }
            else if (skinflag == 5)
            {
                Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
                ofd.Title = "请选择皮肤图片";
                ofd.DefaultExt = ".jpg";
                ofd.ValidateNames = true;
                ofd.CheckPathExists = true;
                ofd.CheckFileExists = true;

                if (ofd.ShowDialog() != null)
                {
                    if (ofd.FileName != "")
                    {
                        Image img = new Image();
                        BitmapImage bmp = new BitmapImage();
                        bmp.BeginInit();//初始化
                        
                       
                        try {
                            bmp.UriSource = new Uri(ofd.FileName);
                            bmp.EndInit();
                        }
                        catch(Exception)
                        {
                            uMessageBox.Show(this,"Keyper", "文件无法作为皮肤图片！");
                            return;
                        }
                        skinflag = 5;
                        using1.Visibility = System.Windows.Visibility.Hidden;
                        using2.Visibility = System.Windows.Visibility.Hidden;
                        using3.Visibility = System.Windows.Visibility.Hidden;
                        using4.Visibility = System.Windows.Visibility.Hidden;
                        using5.Visibility = System.Windows.Visibility.Visible;
                      
                        
                        mainbg.Source = bmp;
                        saveskin(5, ofd.FileName);
                        
                    }
                }
            }
            else
                return;

        
        }

        #endregion

        private void saveskin(int i,string filename) 
        {
            FileStream fs = File.Open(userInfo.stringTObase(currentUser.userName) + @"\" + userInfo.stringTObase("skin.dat"), FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            if (i > 0 && i < 5) 
            {
                bw.Write(i);
                bw.Close();
                fs.Close();
            }
            else if (i == 5)
            {
                bw.Write(i);
                bw.Write(filename);
                bw.Close();
                fs.Close();
            }
            else 
            {
                bw.Close();
                fs.Close();
            }
        }

        private void loadskin() 
        {
            string str1 = userInfo.stringTObase(currentUser.userName) + @"\" + userInfo.stringTObase("skin.dat");
            FileStream fs = File.Open(str1, FileMode.OpenOrCreate);
            BinaryReader br = new BinaryReader(fs);
            try
            {
                int i = br.ReadInt32();
                if (i > 0 && i < 5)
                {
                    Image img = new Image();
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();//初始化
                    using1.Visibility = System.Windows.Visibility.Hidden;
                    using2.Visibility = System.Windows.Visibility.Hidden;
                    using3.Visibility = System.Windows.Visibility.Hidden;
                    using4.Visibility = System.Windows.Visibility.Hidden;
                    using5.Visibility = System.Windows.Visibility.Hidden;
                    switch (i)
                    {
                        case 1:
                            skinflag = 1;
                            mainbg.Source = skinimg01.Source;
                            using1.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 2:
                            skinflag = 2;
                            mainbg.Source = skinimg02.Source;
                            using2.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 3:
                            skinflag = 3;
                            mainbg.Source = skinimg03.Source;
                            using3.Visibility = System.Windows.Visibility.Visible;
                            break;
                        case 4:
                            skinflag = 4;
                            mainbg.Source = skinimg04.Source;
                            using4.Visibility = System.Windows.Visibility.Visible;
                            break;

                    }
                }
                else if (i == 5)
                {
                    string str = br.ReadString();
                    Image img = new Image();
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();//初始化


                    
                    bmp.UriSource = new Uri(str);
                   
                    skinflag = 5;
                    using1.Visibility = System.Windows.Visibility.Hidden;
                    using2.Visibility = System.Windows.Visibility.Hidden;
                    using3.Visibility = System.Windows.Visibility.Hidden;
                    using4.Visibility = System.Windows.Visibility.Hidden;
                    using5.Visibility = System.Windows.Visibility.Visible;
                    bmp.EndInit();

                    mainbg.Source = bmp;
                    
                }
                else
                {
                    skinflag = 1;
                    br.Close();
                    fs.Close();
                }
            }
            catch (Exception) 
            {
                skinflag = 1;
                br.Close();
                fs.Close();
                return;
            }
           
        }

        #endregion


        private void drag2move(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();// 在此处添加事件处理程序实现。// 在此处添加事件处理程序实现。
        }

        private void quit(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void msbu_shengchengmima(object sender, MouseButtonEventArgs e)
        {
            generatePwMessage gpm = new generatePwMessage();
            if ((uMessageBox.Show3(this, "keyper", "生成密码将清空当前输入！",gpm)) == true)
            {
                if (gpm.containflag == false)
                {
                    string gpw = cryptogram.getRandomPassword(gpm.l, 0);
                    tb_add_pwortishi.Text = gpw;
                    int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                    double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                    if (randomnessTestValue < 60)
                    {
                        pwStrengthRect.Fill = brushred;
                        pwtishiLabel.Foreground = brushred;
                        pwtishiLabel.Content = "密码强度过低！";
                    }
                    else if (randomnessTestValue < 75)
                    {
                        pwStrengthRect.Fill = brushblue;
                        pwtishiLabel.Foreground = brushblue;
                        pwtishiLabel.Content = "密码强度合适！";
                    }
                    else if (randomnessTestValue < 101)
                    {
                        pwStrengthRect.Fill = brushgreen;
                        pwtishiLabel.Foreground = brushgreen;
                        pwtishiLabel.Content = "密码强度高！";
                    }

                    pwStrengthRect.Width = pwStrengthRect_Width;
                }
                else 
                {

                    string gpw = cryptogram.getRandomPassword(gpm.l, 1);
                    tb_add_pwortishi.Text = gpw;
                    int randomnessTestValue = cryptogram.randomnessTestValue(tb_add_pwortishi.Text);
                    double pwStrengthRect_Width = randomnessTestValue / 100.0 * 282;
                    if (randomnessTestValue < 60)
                    {
                        pwStrengthRect.Fill = brushred;
                        pwtishiLabel.Foreground = brushred;
                        pwtishiLabel.Content = "密码强度过低！";
                    }
                    else if (randomnessTestValue < 75)
                    {
                        pwStrengthRect.Fill = brushblue;
                        pwtishiLabel.Foreground = brushblue;
                        pwtishiLabel.Content = "密码强度合适！";
                    }
                    else if (randomnessTestValue < 100)
                    {
                        pwStrengthRect.Fill = brushgreen;
                        pwtishiLabel.Foreground = brushgreen;
                        pwtishiLabel.Content = "密码强度高！";
                    }

                    pwStrengthRect.Width = pwStrengthRect_Width;
                }
            }
            else {
                return;
            }
        }

        private void changeUseFingerprint(object sender, MouseButtonEventArgs e)
        {
            if (currentUser.fp.useFP == 0)
            {
                if (uMessageBox.Show2(this, "Keyper", "是否注册指纹？", "取消", "注册") == true) 
                {
                    if ((messagebox_getfingerprint.ShowRegst(this,currentUser)) == true) 
                    {
                        currentUser.fp.useFP = 1;
                        usefingerico.Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }
            else 
            {
                if (uMessageBox.Show2(this, "Keyper", "即将指纹功能？需要验证指纹和主密钥，是否继续", "取消", "继续") == true)
                {
                    if ((messagebox_getfingerprint.ShowClose(this, currentUser)) == true)
                    {
                        currentUser.fp.useFP = 0;
                        currentUser.fp.closeFingerService();
                        usefingerico.Visibility = System.Windows.Visibility.Hidden;
                        uMessageBox.Show(this, "Keyper", "已成功关闭指纹功能！");
                        
                    }
                }
                
            }

        }
#region clipboard
        private void me_copyico(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (tb_add_pwortishi.Text.Length == 0)
                return;

            ico_copy.Opacity = 1;
        }

        private void ml_copyico(object sender, MouseEventArgs e)
        {
            ico_copy.Opacity = 0.39;
        }

        private void md_copyico(object sender, MouseButtonEventArgs e)
        {
            if (tb_add_pwortishi.Text.Length == 0)
                return;

            ico_copy.Opacity = 0.39;
        }

        private void mu_copyico(object sender, MouseButtonEventArgs e)
        {
            ico_copy.Opacity = 1;
            if (tb_add_pwortishi.Text.Length == 0)
                return;
            trycopytoclipboard();
            
        }

        private void trycopytoclipboard() 
        {
            try
            {
                Clipboard.Clear();
                Clipboard.SetDataObject(tb_add_pwortishi.Text);
                //Clipboard.SetText(tb_add_pwortishi.Text);
                clearflag = 1;
                uMessageBox.Show(this, "Keyper", "密码已复制到剪切板，15s后自动销毁！可在设置中修改销毁时间。");

            }
            catch (Exception ec)
            {
                trycopytoclipboard();
            }
        }
        private static int clearflag = 0;
        private static void tryclearclipboard(object o) 
        {
            try
            {
                Clipboard.Clear();
                //Clipboard.SetText(tb_add_pwortishi.Text);


            }
            catch (Exception ec)
            {

                tryclearclipboard(new object());
            }
        }
        private static void clearClipboard() 
        {
            TimeSpan ts1 = new TimeSpan(0, 0, 1);
            TimeSpan ts2 = new TimeSpan(0,0, 15);
            while (true) 
            {
                if (clearflag == 0)
                {
                    Thread.Sleep(ts1);
                }
                else if (clearflag == 2)
                {
                    m_SyncContext.Post(tryclearclipboard, new object());
                    //tryclearclipboard();
                    return;
                }else
                {
                    clearflag = 0;
                    Thread.Sleep(ts2);
                    if (clearflag == 0) 
                    {
                        m_SyncContext.Post(tryclearclipboard,new object());
                        //tryclearclipboard();
                    }
                }
            
            }
           
            
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tryclearclipboard(new object());
            clearClipboardThread.Abort();
        }

        private void me_copyico2(object sender, MouseEventArgs e)
        {
            if (((string)(passwordlabel.Content)).Length == 0)
                return;

            ico_copy2.Opacity = 1;
        }

        private void ml_copyico2(object sender, MouseEventArgs e)
        {
            ico_copy2.Opacity = 0.39;
        }

        private void md_copyico2(object sender, MouseButtonEventArgs e)
        {
            if (((string)(passwordlabel.Content)).Length == 0)
                return;

            ico_copy2.Opacity = 1;
        }

        private void mu_copyico2(object sender, MouseButtonEventArgs e)
        {
            ico_copy2.Opacity = 0.39;
            if (((string)(passwordlabel.Content)).Length == 0)
                return;
            trycopytoclipboard2();
        }
        private void trycopytoclipboard2()
        {
            try
            {
                Clipboard.Clear();
                Clipboard.SetDataObject((string)(passwordlabel.Content));
                //Clipboard.SetText(tb_add_pwortishi.Text);
                clearflag = 1;
                uMessageBox.Show(this, "Keyper", "密码已复制到剪切板，15s后自动销毁！可在设置中修改销毁时间。");

            }
            catch (Exception ec)
            {
                trycopytoclipboard();
            }
        }
#endregion
        private string filepositionstr = "";
        private int filepositionselected = 0;

        private void selectSavePosition(object sender, MouseButtonEventArgs e)
        {

            System.Windows.Forms.FolderBrowserDialog fb = new System.Windows.Forms.FolderBrowserDialog();
            fb.Description = "请选择备份文件存放路径";
            if (fb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filepositionstr = fb.SelectedPath;
                filepositionLabel.Content = filepositionstr;
                filepositionLabel.Opacity = 1;
                imgBegin.Opacity = 1;
                filepositionselected = 1;
                
            }
        }

        private void backupClicked(object sender, MouseButtonEventArgs e)
        {
            if (filepositionselected == 0)
                return;
            imgBegin.Opacity = 1;
            beginbacklabel.Opacity = 1;
            BackUpinfo bui = new BackUpinfo();
            bui.ul = loginWin.userList;
            bui.username = currentUser.userName;
            bui.LocalOrOnline = 0;
            bui.BuOrRec = 0;
            bui.filename = filepositionstr;
            uMessageBox.ShowBackup(this,bui);
        }

        private void backupClicked_down(object sender, MouseButtonEventArgs e)
        {
            if (filepositionselected == 0)
                return;
            imgBegin.Opacity = 0.55;
            beginbacklabel.Opacity = 0.55;
        }
        private string RecPosition = "";
        private int RecPositionSelected = 0;
        private void selectRecPosition(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Title = "请选择备份文件(kpb)";

            ofd.ValidateNames = true;
            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;
            ofd.Filter="Keyper备份文件(*.kpb)|*.kpb";
            int n;
            if (ofd.ShowDialog() != null)
            {
                if (ofd.FileName != "")
                {
                    filenameLabel.Opacity = 1;
                    RecPosition = ofd.FileName;
                    filenameLabel.Content = RecPosition;
                    RecPositionSelected = 1;
                    imgright.Opacity = 1;
                    beginrecoverylabel.Opacity = 1;
                }
                else
                {
                    filenameLabel.Opacity = 0.55;
                    filenameLabel.Content = "点击浏览文件";
                    RecPositionSelected = 0;
                    imgright.Opacity = 0.55;
                    beginrecoverylabel.Opacity = 0.55;
                }
            }
            else
            {
                filenameLabel.Opacity = 0.55;
                filenameLabel.Content = "点击浏览文件";
                RecPositionSelected = 0;
                imgright.Opacity = 0.55;
                beginrecoverylabel.Opacity = 0.55;
            }
        }

        private void recClicked(object sender, MouseButtonEventArgs e)
        {
            if (RecPositionSelected == 0)
                return;
            beginrecoverylabel.Opacity = 1;
            imgright.Opacity = 1;
            if (File.Exists(RecPosition) == true)
            {
              
                imgBegin.Opacity = 1;
                beginbacklabel.Opacity = 1;
                BackUpinfo bui = new BackUpinfo();
                bui.ul = loginWin.userList;
                bui.username = currentUser.userName;
                bui.LocalOrOnline = 0;
                bui.BuOrRec = 1;
                bui.filename = RecPosition;
                if (uMessageBox.ShowBackup(this, bui) == true) 
                {
                    userInfo.readUserInfoFromFile2();
                    (new Thread(refreshULThreadFounc)).Start();
                }
            }
            else 
            {
                uMessageBox.Show(this, "Keyper", "文件不存在！");
                filenameLabel.Opacity = 0.55;
                filenameLabel.Content = "点击浏览文件";
                RecPositionSelected = 0;
                imgright.Opacity = 0.55;
                beginrecoverylabel.Opacity = 0.55;
            }
        }
        private void refreshULThreadFounc()
        {
            userInfo.readUserInfoFromFile2();
            m_SyncContext.Post(refreshULWindow, 0);
        }

        private void refreshULWindow(object o) 
        {
            refreshMainwidow();
        
        }
        private void recClicked_down(object sender, MouseButtonEventArgs e)
        {
            if (RecPositionSelected == 0)
                return;
            imgright.Opacity = 0.55;
            beginrecoverylabel.Opacity = 0.55;
        }

        private void minsize(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

       

       
    }
    public class securitystring 
    {
        public string str = "";
    
    }










   
}
